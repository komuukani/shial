<?php
/* * *
 * Project:    Dhillon Printing
 * Name:       Admin Side Pages
 * Package:    Pages.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2023
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    // addToCart

    public function getIPAddress()
    {
        return $this->input->cookie('unique_id');
    }

    // Get Visitor IP Address

    public function page_not_found()
    {
        $data = $this->setParameter(__FUNCTION__, "Page Not Found", false);
        $this->load->view('master', $data);
    }

    /*
      P A G E S
     */

    // Page not found 404

    protected function setParameter($page, $title, $page_breadcumb)
    {
        // generate unique_id
        if (!$this->input->cookie('unique_id')) {
            $this->load->helper('cookie');
            $uid = $this->input->cookie('ci_session');
            set_cookie('unique_id', $uid, 60 * 60 * 24 * 365);
        }
        $data = [];
        $data['page_title'] = $title;
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = $this->website;
        $data['developer'] = $this->developer;
        $data['web_link'] = $this->web_link;
        $data['page_breadcumb'] = $page_breadcumb;
        $data['page'] = $page;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Home Page
    public function index()
    {
        $data = $this->setParameter(__FUNCTION__, "Home", false);
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('fname', 'First Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('total_guests', '', 'required', array("required" => "Select Total Guests!"));
            $this->form_validation->set_rules('datetime', '', 'required', array("required" => "Select Date and Time!"));
            $this->form_validation->set_rules('food', '', 'required', array("required" => "Select Food Type!"));
            if ($this->form_validation->run() == TRUE) {
//                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
//                if ($captchaStatus) {
                $insert_data['fname'] = $this->input->post('fname');
                $insert_data['email'] = $this->input->post('email');
                $insert_data['phone'] = $this->input->post('phone');
                $insert_data['total_guests'] = $this->input->post('total_guests');
                $insert_data['datetime'] = $this->input->post('datetime');
                $insert_data['food'] = $this->input->post('food');
                $insert_data['entry_date'] = date('Y-m-d H:i:s');
                if ($this->md->insert($this->table_prefix . __FUNCTION__, $insert_data)) {
                    $data['success'] = 'Reservation Request sent successfully.';
                } else {
                    $data['error'] = 'Sorry, Reservation Request not submitted!';
                }
//                } else {
//                    $data['error'] = 'Oops, Validate google reCaptcha!';
//                }
            }
        }
        $this->load->view('master', $data);
    }

    // Contact Page
    public function contact()
    {
        $data = $this->setParameter(__FUNCTION__, "Get in Touch", true);
        if ($this->input->post('send')) {
            $this->form_validation->set_rules('fname', 'First Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('message', 'Message', 'required', array("required" => "Enter Message!"));
            if ($this->form_validation->run() == TRUE) {
                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                if ($captchaStatus) {
                    if ($this->input->post('extra_field')) {
                        $data['error'] = 'Sorry, You are a bot!';
                    } else {
                        $insert_data['name'] = $this->input->post('fname');
                        $insert_data['email'] = $this->input->post('email');
                        $insert_data['phone'] = $this->input->post('phone');
                        $insert_data['message'] = $this->input->post('message');
                        $insert_data['datetime'] = date('Y-m-d H:i:s');
                        if ($this->md->insert($this->table_prefix . __FUNCTION__, $insert_data)) {

                            // Send Notification to admin for New Form added.
                            $templateData = array(
                                "fname" => $this->input->post('fname'),
                                "email" => $this->input->post('email'),
                                "phone" => $this->input->post('phone'),
                                "message" => $this->input->post('message')
                            );
                            $adminEmail = "nishantthummar005@gmail.com";
                            $this->md->sendMailSMTP2Go($adminEmail, '', '', '4932765', $templateData);

                            $data['success'] = 'Contact Submitted successfully.';
                        } else {
                            $data['error'] = 'Sorry, Contact not submitted!';
                        }
                    }
                } else {
                    $data['error'] = 'Oops, Validate google reCaptcha!';
                }
            }
        }
        $this->load->view('master', $data);
    }

    // Free Consultancy Page
    public function free_consultancy()
    {
        $data = $this->setParameter(__FUNCTION__, "Free Consultancy", true);
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('fname', 'First Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('slotDate', '', 'required', array("required" => "Select Slot!"));
            $this->form_validation->set_rules('slot_time', '', 'required', array("required" => "Select Slot Time!"));
            if ($this->form_validation->run() == TRUE) {
//                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
//                if ($captchaStatus) {

                $slots = $this->md->select_where('tbl_slot', array('date' => $this->input->post('slotDate'), 'time' => $this->input->post('slot_time')));
                if ($slots) {
                    $insert_data['slot_id'] = $slots[0]->slot_id;
                }

                $insert_data['slot_date'] = $this->input->post('slotDate');
                $insert_data['slot_time'] = $this->input->post('slot_time');
                $insert_data['fname'] = $this->input->post('fname');
                $insert_data['email'] = $this->input->post('email');
                $insert_data['phone'] = $this->input->post('phone');
                $insert_data['message'] = $this->input->post('message');
                $insert_data['status'] = 1;
                $insert_data['entry_date'] = date('Y-m-d');
                if ($this->md->insert($this->table_prefix . 'book', $insert_data)) {

                    // Send Notification to admin for New Form added.
//                    $templateData = array(
//                        "fname" => $this->input->post('fname'),
//                        "email" => $this->input->post('email'),
//                        "phone" => $this->input->post('phone'),
//                        "total_guests" => $this->input->post('total_guests'),
//                        "datetime" => $this->input->post('datetime'),
//                        "food" => $this->input->post('food')
//                    );
//                    $adminEmail = "nishantthummar005@gmail.com";
//                    $this->md->sendMailSMTP2Go($adminEmail, '', '', '5602740', $templateData);

                    $data['success'] = 'Request sent successfully.';
                } else {
                    $data['error'] = 'Sorry, Request not submitted!';
                }
//                } else {
//                    $data['error'] = 'Oops, Validate google reCaptcha!';
//                }
            }
        }
        $this->load->view('master', $data);
    }

    // Aboutus Page
    public function aboutus()
    {
        $data = $this->setParameter(__FUNCTION__, "About us", true);
        $this->load->view('master', $data);
    }

    // Privacy policy Page
    public function policy()
    {
        $data = $this->setParameter(__FUNCTION__, "Privacy & Policy", true);
        $this->load->view('master', $data);
    }

    // Terms & Condition Page
    public function terms()
    {
        $data = $this->setParameter(__FUNCTION__, "Terms & Condition", true);
        $this->load->view('master', $data);
    }

    // Faq Page
    public function faq()
    {
        $data = $this->setParameter(__FUNCTION__, "Frequently Asked Questions", true);
        $this->load->view('master', $data);
    }

    // services Page
    public function services()
    {
        $data = $this->setParameter(__FUNCTION__, "Our Services", true);
        $this->load->view('master', $data);
    }

    // Gallery Page
    public function gallery()
    {
        $data = $this->setParameter(__FUNCTION__, "Gallery", true);
        $this->load->view('master', $data);
    }

    // Package Page
    public function package()
    {
        $data = $this->setParameter(__FUNCTION__, "Package", true);
        $this->load->view('master', $data);
    }

    // Cart Page
    public function cart()
    {
        $data = $this->setParameter(__FUNCTION__, "Cart", true);
        $this->load->view('master', $data);
    }

    // Checkout Page
    public function checkout()
    {
        $data = $this->setParameter(__FUNCTION__, "Checkout", true);
        $this->load->view('master', $data);
    }

    // Menu Page
    public function menu()
    {
        $data = $this->setParameter(__FUNCTION__, "Menu", true);
        $this->load->view('master', $data);
    }

    // Newsletter Page
    public function newsletter()
    {
        if ($this->input->post('newsletter')) {
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[tbl_email_subscriber.email]', array("required" => "Enter Email Address!"));
            if ($this->form_validation->run() == TRUE) {
                $insert_data['email'] = $this->input->post('email');
                $insert_data['datetime'] = date('Y-m-d H:i:s');
                if ($this->md->insert($this->table_prefix . 'email_subscriber', $insert_data)) {
                    $this->session->set_flashdata('newsletter', 'Email Registered successfully.');
                    redirect($_SERVER['HTTP_REFERER'], 'refresh');
                } else {
                    $data['error'] = 'Sorry, Email not registered!';
                    redirect($_SERVER['HTTP_REFERER'], 'refresh');
                }
            } else {
                redirect($_SERVER['HTTP_REFERER'], 'refresh');
            }
        }
    }


    // Add to cart in cart
    public function addToCart()
    {
        $insert_data['package_id'] = $this->input->post('package_id');
        $insert_data['unique_id'] = $this->getIPAddress();
        $checkExist = $this->md->select_where($this->table_prefix . 'cart', $insert_data);
        if ($checkExist) {
            // Update
            $up['price'] = $this->input->post('price');
            $up['qty'] = $this->input->post('qty');
            $up['netprice'] = $this->input->post('price') * $this->input->post('qty');
            $up['modify_date'] = date('Y-m-d H:i:s');
            $this->md->update($this->table_prefix . 'cart', $up, $insert_data);
        } else {
            // Insert
            $insert_data['price'] = $this->input->post('price');
            $insert_data['qty'] = $this->input->post('qty');
            $insert_data['netprice'] = $this->input->post('price') * $this->input->post('qty');
            $insert_data['entry_date'] = date('Y-m-d H:i:s');
            $this->md->insert($this->table_prefix . 'cart', $insert_data);
        }
        redirect($_SERVER['HTTP_REFERER']);

    }

    // Update QTY in cart
    public function updateQty()
    {
        $cartid = $this->input->post('cartId');
        $quantity = $this->input->post('demo_vertical2');
        if ($cartid != "" && $quantity != "") {
            $cartData = $this->md->select_where('tbl_cart', array('cart_id' => $cartid));
            if ($cartData) {
                if ($quantity == 0) {
                    $this->md->delete('tbl_cart', array('cart_id' => $cartid));
                    $data['success'] = 'Item Removed from cart!';
                } else {
                    $up['unique_id'] = $this->getIPAddress();
                    $up['modify_date'] = date('Y-m-d H:i:s');
                    $up['qty'] = $quantity;
                    $up['netprice'] = $quantity * $cartData[0]->price;
                    $this->md->update('tbl_cart', $up, array('cart_id' => $cartid));
                    $data['success'] = 'Cart updated successfully.';
                }
            } else {
                $data['error'] = 'Sorry, Data not found!';
            }
        } else {
            $data['error'] = 'Sorry, Data not found!';
        }

        redirect('cart');
    }


    // REMOVE CART ITEM
    public function removeToCart()
    {
        $cart_id = $this->input->post('cartId');  // Cart ID
        $exist = $this->md->select_where('tbl_cart', array('cart_id' => $cart_id));
        if (!empty($exist)) {
            $this->md->delete('tbl_cart', array('cart_id' => $cart_id));
            echo 1; // deleted
        } else {
            echo 2; // item not found
        }
    }

    // GET SLOT TIME FROM SLOT ID
    public function getSlotTime()
    {
        $slotDate = $this->input->post('slotDate');  // slot Date
        $slots = $this->md->select_where('tbl_slot', array('date' => $slotDate));
        echo '<option value="">Select Slot time</option>';
        if ($slots) {
            foreach ($slots as $slot) {
                $totalSlot = $slot->slot;
                $bookExist = $this->md->select_where('tbl_book', array('slot_id' => ($slot ? $slot->slot_id : ''), 'slot_date' => $slotDate, 'slot_time' => $slot->time));
                if ($totalSlot > count($bookExist)) {
                    echo '<option value="' . $slot->time . '">' . date('h:i A', strtotime($slot->time)) . '</option>';
                }
            }
        }
    }


}
