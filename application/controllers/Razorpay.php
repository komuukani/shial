<?php

//  Front side
//  Author          :   Theexpertguy
//  Author URi      :   http://theexpertguy.com/
//  Template Name   :   Ecommerce Platform
//  Version         :   2.1
//  File            :   Pages Controller

defined('BASEPATH') or exit('No direct script access allowed');

class Razorpay extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_)
        // Check if database already connected or not [STATUS = PRE/POST]
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    public function getIPAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function index()
    {
        $this->checkout();
    }

    public function checkout()
    {
        $data['title'] = 'Checkout payment';
        $data['callback_url'] = base_url() . 'razorpay/callback';
        $data['surl'] = base_url() . 'razorpay/success';
        $data['furl'] = base_url() . 'razorpay/failed';
        $data['currency_code'] = 'INR';
        $this->load->view('razor_demo', $data);
    }


    protected function setParameter($page, $title, $page_breadcumb)
    {
        // generate unique_id
        if (!$this->input->cookie('unique_id')) {
            $this->load->helper('cookie');
            $uid = $this->input->cookie('ci_session');
            set_cookie('unique_id', $uid, 60 * 60 * 24 * 365);
        }
        $data = [];
        $data['page_title'] = $title;
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = $this->website;
        $data['location'] = array('');
        $data['developer'] = $this->developer;
        $data['web_link'] = $this->web_link;
        $data['page_breadcumb'] = $page_breadcumb;
        $data['page'] = $page;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // initialized cURL Request
    private function curl_handler($payment_id, $amount)
    {
        $url = 'https://api.razorpay.com/v1/payments/' . $payment_id . '/capture';
        $key_id = RAZOR_KEYID;  // razor pay key id
        $key_secret = RAZOR_KEY_SECRET;  // razor pay secret key id
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id . ':' . $key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        return $ch;
    }

    private function addBill($response_array)
    {
        $ip = $this->getIPAddress();
        $user = $this->session->userdata('email');
        $user_Type = $this->session->userdata('userType');
        $user_data = $cart = array();
        if (isset($user)) :
            $user_data = $this->md->select_where('tbl_register', array('email' => $user));
            if ($user_Type == 'guest') {
                $cart = $this->md->select_where('tbl_cart', array('email' => $user));
            } else {
                $cart = $this->md->select_where('tbl_cart', array('register_id' => ($user_data ? $user_data[0]->register_id : '')));
            }
        else:
            $cart = $this->md->select_where('tbl_cart', array('unique_id' => $this->getIPAddress()));
        endif;
        if (empty($cart)) {
            redirect('404');
        } else {

            $net = 0;
            if (!empty($cart)) {
                foreach ($cart as $cart_data) {
                    $net = $net + $cart_data->netprice;
                }
            }

            // Bill Entry
            if ($user_Type != 'guest') {
                $register_id = $user_data[0]->register_id;
                $bill_data['register_id'] = $register_id;
                $bill_data['user_type'] = 'regular';
            } else {
                $bill_data['user_type'] = 'guest';
            }
            $bill_data['unique_id'] = $ip;
            $bill_data['fname'] = $this->input->post('fname');
            $bill_data['email'] = $this->input->post('email');
            $bill_data['phone'] = $this->input->post('phone');
            $bill_data['transaction_id'] = $this->session->userdata('razorpay_payment_id') ? $this->session->userdata('razorpay_payment_id') : '';
            $bill_data['order_id'] = $this->session->userdata('merchant_order_id') ? $this->session->userdata('merchant_order_id') : '';
            $bill_data['country'] = $this->input->post('country');
            $bill_data['state'] = $this->input->post('state');
            $bill_data['city'] = $this->input->post('city');
            $bill_data['address'] = $this->input->post('address');
            $bill_data['postal_code'] = $this->input->post('postal_code');
            $bill_data['netprice'] = $net;
            $bill_data['notes'] = $this->input->post('notes');
            if (array_key_exists('card', $response_array)) {
                $bill_data['card'] = json_encode($response_array['card']);
            }
            if (array_key_exists('upi', $response_array)) {
                $bill_data['upi_id'] = json_encode($response_array['upi']);
                $bill_data['upi_transaction_id'] = $response_array['acquirer_data']['upi_transaction_id'];
            }
            $bill_data['payment_status'] = $response_array['status'];
            $bill_data['entry_date'] = date('Y-m-d H:i:s');
            if ($this->md->insert('tbl_bill', $bill_data)) {
                // Last Bill ID
                $bill_id = $this->db->insert_id(); // Last Bill ID
                if (!empty($cart)) {
                    foreach ($cart as $cart_data) {
                        if (!empty($bill_id)) {
                            // Transaction Entry
                            $tra_data['bill_id'] = $bill_id;
                            $tra_data['product_id'] = $cart_data->product_id;
                            $tra_data['price'] = $cart_data->price;
                            $tra_data['qty'] = $cart_data->qty;
                            $tra_data['netprice'] = $cart_data->netprice;
                            if ($user_Type != 'guest') {
                                $register_id = $user_data[0]->register_id;
                                $tra_data['register_id'] = $register_id;
                            } else {
                                $tra_data['email'] = $user;
                            }
                            $tra_data['entry_date'] = date('Y-m-d');

                            $this->md->insert('tbl_transaction', $tra_data);
                        }
                    }
                    $wh = array();
                    if ($user_Type != 'guest') {
                        $register_id = $user_data[0]->register_id;
                        $wh['register_id'] = $register_id;
                    } else {
                        $wh['email'] = $user;
                    }
                    if ($this->md->delete('tbl_cart', $wh)) {
                        $data['success'] = 'Order Placed successfully.';
                        if ($user_Type == 'guest') {
                            $this->session->unset_userdata('email');
                        }
                        $this->success();
                        //redirect('success');
                    } else {
                        $data['error'] = 'Sorry, something went wrong!';
                    }
                }
            } else {
                $data['error'] = 'Sorry, something went wrong!';
            }

        }
    }

    // callback method
    public function callback()
    {
//        print_r($this->input->post());
//        die;
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');

            $this->session->set_userdata('razorpay_payment_id', $this->input->post('razorpay_payment_id'));
            $this->session->set_userdata('merchant_order_id', $this->input->post('merchant_order_id'));
            $currency_code = 'INR';
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = $response_array = '';
            try {
                $ch = $this->curl_handler($razorpay_payment_id, $amount);
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: ' . curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                    //Check success response
                    if ($http_status === 200 and isset($response_array['error']) === false) {
                        $success = true;
                    } else {
                        $success = false;
                        if (!empty($response_array['error']['code'])) {
                            $error = $response_array['error']['code'] . ':' . $response_array['error']['description'];
                        } else {
                            $error = 'RAZORPAY_ERROR:Invalid Response <br/>' . $result;
                        }
                    }
                }
                //close curl connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'Request to Razorpay Failed';
            }

            if ($success === true) {
                if (!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                }
//                    if (!$order_info['order_status_id']) {
//                        $this->addBill();   // add bill entry with Online Payment
//                        redirect($this->input->post('merchant_surl_id'));
//                    } else {
//                        $this->addBill();   // add bill entry with Online Payment
//                        redirect($this->input->post('merchant_surl_id'));
//                    }
                $this->addBill($response_array);   // add bill entry with Online Payment
                redirect($this->input->post('merchant_surl_id'));
            } else {
                redirect($this->input->post('merchant_furl_id'));
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    }

    public function success()
    {
        $data = [];
        $data = $this->setParameter('success', "Order Placed Successfully", false);
        $data['msg'] = "success";
        $data['trans_id'] = $this->session->userdata('razorpay_payment_id');
        $data['order_id'] = $this->session->userdata('merchant_order_id');
//        $this->session->unset_userdata('razorpay_payment_id');
//        $this->session->unset_userdata('merchant_order_id');
        $this->load->view('master', $data);
    }

    public function failed()
    {
        $data = [];
        $data = $this->setParameter('success', "Payment Failed", false);
        $data['msg'] = "failed";
        $data['trans_id'] = $this->session->userdata('razorpay_payment_id');
        $data['order_id'] = $this->session->userdata('merchant_order_id');
//        $this->session->unset_userdata('razorpay_payment_id');
//        $this->session->unset_userdata('merchant_order_id');
        $this->load->view('master', $data);
    }

}
