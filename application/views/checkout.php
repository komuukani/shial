<?php
echo $page_head;
$unique_id = $this->input->cookie('unique_id');
$cartData = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
$ip = $this->md->getVisIpAddr();
$ipdat = @json_decode(file_get_contents(
    "http://www.geoplugin.net/json.gp?ip=" . $ip));
$currency_fromat = (!empty($ipdat->geoplugin_countryName) && ($ipdat->geoplugin_countryName != 'India')) ? '$' : "₹";
?>
<body id="bg" class="data-typography-1">

<div class="page-wraper">
    <?php echo $page_header; ?>

    <div class="page-content bg-white">
        <?php echo $page_breadcumb; ?>

        <div class="content-inner-1">
            <div class="container">
                <div class="row shop-checkout">
                    <div class="col-xl-8">
                        <h5 class="title m-b15 text-uppercase font-weight-700" data-wow-delay="0.2s">Billing
                            details</h5>

                        <form
                            name="razorpay-form"
                            id="razorpay-form"
                            action="<?php echo base_url('Razorpay/callback'); ?>"
                            class="row m-t30" method="post">
                            <?php
                            ##
                            # RAZORPAY PAYMENT GATEWAY CONFIGURATION
                            ##
                            $net = 15;
                            $description = "Package Purchased";
                            $txnid = date("YmdHis");
                            $key_id = RAZOR_KEYID;
                            $currency_code = $ipdat->geoplugin_countryName != 'India' ? "INR" : "USD";
                            $total = ($net * 100);   // 100 = 1 indian rupees
                            $amount = $net;
                            $merchant_order_id = uniqid();
                            $card_holder_name = "Nishant Thummar";
                            $email = 'nishantthummar005@gmail.com';
                            $phone = "8460124263";
                            $name = "Shial";

                            if (isset($error)) {
                                ?>
                                <div class="alert alert-danger p-1">
                                    <?php echo $error; ?>
                                </div>
                                <?php
                            }
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success p-1">
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-md-12">
                                <div class="form-group m-b20" data-wow-delay="0.5s">
                                    <label class="label-title">Your Name</label>
                                    <input name="fname" required="" class="form-control"
                                           id="first-name"
                                           value="<?php
                                           if (set_value('fname') && !isset($success)) {
                                               echo set_value('fname');
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-b25" data-wow-delay="1.3s">
                                    <label class="label-title">Phone *</label>
                                    <input name="phone" type="number" inputmode="numeric" required=""
                                           id="phone"
                                           class="form-control"
                                           value="<?php
                                           if (set_value('phone') && !isset($success)) {
                                               echo set_value('phone');
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-b25" data-wow-delay="1.4s">
                                    <label class="label-title">Email address *</label>
                                    <input name="email" id="email" type="email" required="" class="form-control"
                                           value="<?php
                                           if (set_value('email') && !isset($success)) {
                                               echo set_value('email');
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="m-b25" data-wow-delay="0.8s">
                                    <label class="label-title">Country / Region *</label>
                                    <input name="country" required="" id="country" class="form-control m-b15"
                                           value="<?php
                                           if (set_value('country') && !isset($success)) {
                                               echo set_value('country');
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="m-b25" data-wow-delay="0.8s">
                                    <label class="label-title">Country / Region *</label>
                                    <input name="city" required="" id="city" class="form-control m-b15"
                                           value="<?php
                                           if (set_value('city') && !isset($success)) {
                                               echo set_value('city');
                                           }
                                           ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group m-b25" data-wow-delay="0.9s">
                                    <label class="label-title">Street address *</label>
                                    <input name="address" required="" class="form-control m-b15"
                                           id="street-address"
                                           value="<?php
                                           if (set_value('address') && !isset($success)) {
                                               echo set_value('address');
                                           }
                                           ?>"
                                           placeholder="House number and street name">
                                </div>
                            </div>

                            <input type="hidden" name="amount" value="<?php echo $net; ?>">
                            <input type="hidden" name="razorpay_payment_id"
                                   id="razorpay_payment_id"/>
                            <input type="hidden" name="merchant_order_id" id="merchant_order_id"
                                   value="<?php echo $merchant_order_id; ?>"/>
                            <input type="hidden" name="merchant_trans_id" id="merchant_trans_id"
                                   value="<?php echo date("YmdHis"); ?>"/>
                            <input type="hidden" name="merchant_product_info_id"
                                   id="merchant_product_info_id"
                                   value="<?php echo $description; ?>"/>
                            <input type="hidden" name="merchant_surl_id" id="merchant_surl_id"
                                   value="<?php echo base_url('razorpay/success'); ?>"/>
                            <input type="hidden" name="merchant_furl_id" id="merchant_furl_id"
                                   value="<?php echo base_url('razorpay/failed'); ?>"/>
                            <input type="hidden" name="card_holder_name_id"
                                   id="card_holder_name_id"
                                   value="<?php echo $card_holder_name; ?>"/>
                            <input type="hidden" name="merchant_total" id="merchant_total"
                                   value="<?php echo $total; ?>"/>
                            <input type="hidden" name="merchant_amount" id="merchant_amount"
                                   value="<?php echo $amount; ?>"/>
                        </form>
                    </div>
                    <div class="col-xl-4 side-bar">
                        <h4 class="title m-b15 text-uppercase" data-wow-delay="0.2s">Your Order</h4>
                        <div class="order-detail sticky-top">
                            <?php
                            $cart_total = 0;
                            if (!empty($cartData)) {
                                foreach ($cartData as $cartData_data) {
                                    $package_data = $this->md->select_where('tbl_package', array('package_id' => $cartData_data->package_id), 'LIMIT 1');
                                    $total = $cartData_data->price * $cartData_data->qty;
                                    $cart_total += $total;
                                    ?>
                                    <div class="cart-item style-1">
                                        <div class="dz-media">
                                            <img
                                                src="<?php echo base_url($package_data[0]->path ? $package_data[0]->path : FILENOTFOUND); ?>"
                                                alt="<?php echo $package_data[0]->title; ?>">
                                        </div>
                                        <div class="dz-content">
                                            <h6 class="title mb-0"><a
                                                    href="<?php echo base_url('package'); ?>"><?php echo $package_data[0]->title; ?></a>
                                            </h6>
                                            <span
                                                class="price"><?php echo $currency_fromat . '' . $cartData_data->price; ?></span>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                echo "<tr><td colspan='6' align='center' class='alert alert-danger'>Sorry, You have not added any package!</td></tr>";
                            }
                            ?>
                            <table class="">
                                <tbody>
                                <tr class="subtotal">
                                    <td>Subtotal</td>
                                    <td class="price"><?php echo $currency_fromat . '' . $cart_total; ?></td>
                                </tr>
                                <tr class="total">
                                    <td>Total</td>
                                    <td class="price"><?php echo $currency_fromat . '' . $cart_total; ?></td>
                                </tr>
                                </tbody>
                            </table>
                            <button
                                id="pay-btn" type="button"
                                onclick="razorpaySubmit(this);" name="send"
                                value="place order" name="place_order"
                                class="btn btn-secondary w-100"
                                form="razorpay-form">PLACE ORDER
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<!--
RAZORPAY PAYMENT GATEWAY START
-->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>

    var options = {
        key: "<?php echo $key_id; ?>",
        amount: "<?php echo $total; ?>",
        name: "<?php echo $name; ?>",
        description: "Order # <?php echo $merchant_order_id; ?>",
        netbanking: true,
        currency: "<?php echo $currency_code; ?>", // INR
        prefill: {
            name: "<?php echo $card_holder_name; ?>",
            email: "<?php echo $email; ?>",
            contact: "<?php echo $phone; ?>"
        },
        notes: {
            soolegal_order_id: "<?php echo $merchant_order_id; ?>",
        },
        handler: function (transaction) {
            document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
            document.getElementById('razorpay-form').submit();
        },
        "modal": {
            "ondismiss": function () {
                location.reload();
            }
        }
    };

    var razorpay_pay_btn, instance;

    function razorpaySubmit(el) {
        var firstName = $('#first-name').val();
        var phone = $('#phone').val();
        var email = $('#email').val();
        var streetAddress = $('#street-address').val();
        var country = $('#country').val();
        var city = $('#city').val();

        if (firstName !== '' && phone !== '' && email != '' && streetAddress != '' && country != '' && city != '') {
            if (typeof Razorpay == 'undefined') {
                setTimeout(razorpaySubmit, 200);
                if (!razorpay_pay_btn && el) {
                    razorpay_pay_btn = el;
                    el.disabled = true;
                    el.value = 'Please wait...';
                }
            } else {
                if (!instance) {
                    instance = new Razorpay(options);
                    if (razorpay_pay_btn) {
                        razorpay_pay_btn.disabled = false;
                        razorpay_pay_btn.value = "Pay Now";
                    }
                }
                instance.open();
            }
        } else {
            alert('Fillout all the fields properly!');
        }
    }
</script>
<!--
RAZORPAY PAYMENT GATEWAY END
-->
</body>
