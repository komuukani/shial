<?php
echo $page_head;
?>
<body>
<div id="wrapper">
    <?php echo $page_header; ?>
    <?php echo $page_breadcumb; ?>
    <div id="content">
        <!-- Blog list
============================================= -->
        <section class="blog_list text-left padding-100">
            <div class="container">
                <div class="row">
                    <!-- Blog main content -->
                    <div class="blog-main-content">
                        <?php
                        if (!empty($blog)) {
                            foreach ($blog as $blog_data) {
                                $url = base_url('blog/' . strtolower($blog_data->slug));
                                $blog_desc = substr($blog_data->description, 0, 400);
                                ?>
                                <!-- Blog row -->
                                <div class="blog_row">
                                    <!-- Blog img -->
                                    <figure class="blog-img col-md-6 col-sm-6 col-xs-12"><a
                                            href="<?php echo $url; ?>"><img class="img-responsive"
                                                                            src="<?php echo base_url($blog_data->path); ?>"
                                                                            width="100%"
                                                                            style="height: 400px;object-fit: cover"
                                                                            alt="<?php echo $blog_data->title; ?>"/></a>
                                        <figcaption class="text-center"><span
                                                class="btn btn-gold primary-bg white"><?php echo date('d F Y', strtotime($blog_data->blogdate)); ?></span>
                                        </figcaption>
                                    </figure>
                                    <!-- Blog content -->
                                    <div class="blog-content col-md-6 col-sm-6 col-xs-12">
                                        <h2><a href="<?php echo $url; ?>"><?php echo $blog_data->title; ?></a></h2>
                                        <!-- Links -->
                                        <div class="links">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-user"></i> By Admin</a></li>
                                            </ul>
                                        </div>
                                        <!-- End links -->
                                        <div class="text-content">
                                            <p><?php echo $blog_desc ? $blog_desc : ''; ?></p>
                                            <a href="<?php echo $url; ?>" class="btn btn-gold" data-toggle="tooltip"
                                               data-placement="right" title="Read More"><i
                                                    class="fa fa-arrow-right"></i></a></div>
                                    </div>
                                    <!-- End Blog content -->
                                    <!-- Divider -->
                                    <div class="blog-divider"><span></span> <i class="icon-home-ico"></i> <span></span>
                                    </div>
                                    <!-- End# Divider -->
                                </div>
                                <!-- End Blog row -->
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <!-- End Blog main content -->
                </div>
            </div>
        </section>
        <!-- End blog list -->

        <?php echo $page_footer; ?>
    </div>
    <?php echo $page_footerscript; ?>
</div>
</body>