<?php
echo $page_head;
$reviews = $this->md->select('tbl_review');

?>
<body id="bg" class="data-typography-1">
<div class="page-wraper">
    <?php echo $page_header; ?>

    <div class="page-content bg-white">
        <?php echo $page_breadcumb; ?>

        <section class="content-inner-1">
            <div class="container">
                <div class="clearfix" id="lightgallery">
                    <ul id="masonry1" class="row dlab-gallery-listing gallery-grid-3 sp10 gallery text-center">
                        <?php
                        if (!empty($gallery)) {
                            foreach ($gallery as $gallery_data) {
                                ?>
                                <li class="card-container m-b10 col-lg-4">
                                    <span  data-src="<?php echo base_url($gallery_data->path ? $gallery_data->path : FILENOTFOUND); ?>"
                                       class="dz-media lg-item">
                                        <img src="<?php echo base_url($gallery_data->path ? $gallery_data->path : FILENOTFOUND); ?>"
                                             alt="<?php echo $gallery_data->title; ?>" width="800" height="650">
                                    </span>
                                </li>
                            <?php }
                        } ?>
                    </ul>
                </div>
            </div>
        </section>

    </div>
    <?php echo $page_footer; ?>
</div>
<script src="assets/vendor/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
<script src="assets/vendor/masonry/masonry-4.2.2.js"></script><!-- MASONRY -->
<script src="assets/vendor/masonry/isotope.pkgd.min.js"></script><!-- MASONRY -->
<script src="assets/vendor/lightgallery/dist/lightgallery.min.js"></script>
<script src="assets/vendor/lightgallery/dist/plugins/thumbnail/lg-thumbnail.min.js"></script>
<script src="assets/vendor/lightgallery/dist/plugins/zoom/lg-zoom.min.js"></script>
<?php echo $page_footerscript; ?>

</body>


<!-- end of #content -->
