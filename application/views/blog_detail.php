<?php
echo $page_head;
$web_data = ($web_data) ? $web_data[0] : '';
$blogs = $this->md->select_limit_order('tbl_blog', 5, 'blog_id', 'desc');
?>
<?php
echo $page_head;
?>
<body>
<div id="wrapper">
<?php echo $page_header; ?>
<?php echo $page_breadcumb; ?>
    <div id="content">
        <!-- Blog list
    ============================================= -->
        <section class="blog_single text-left padding-100">
            <div class="container">
                <div class="row">
                    <!-- Blog main content -->
                    <div class="blog-main-content">
                        <!-- Blog row -->
                        <div class="blog_row">
                            <!-- Blog img -->
                            <figure class="blog-img col-md-12"> <img class="img-responsive" src="<?php echo base_url($blog_data ? $blog_data[0]->path : FILENOTFOUND); ?>"
                                                                     alt="<?php echo $blog_data ? $blog_data[0]->title : ''; ?>"
                                                                     title="<?php echo $blog_data ? $blog_data[0]->title : ''; ?>"> </figure>
                            <!-- End Blog img -->
                            <!-- Blog content -->
                            <div class="blog-content col-md-12">
                                <h2><?php echo $blog_data ? $blog_data[0]->title : ''; ?></h2>
                                <!-- Links -->
                                <div class="links">
                                    <ul>
                                        <li><i class="fa fa-calendar"></i> Added on <?php echo date('d F Y', strtotime($blog_data ? $blog_data[0]->blogdate : '')); ?></li>
                                        <li> <a href="#"><i class="fa fa-user"></i> By Admin</a></li>
                                    </ul>
                                </div>
                                <!-- End links -->
                                <div class="text-content">
                                    <p><?php echo $blog_data ? $blog_data[0]->description : ''; ?></p>
                            </div>
                            <!-- End Blog content -->
                        </div>
                        <!-- End Blog row -->
                    </div>
                    <!--End Blog main content -->
                </div>
            </div>
        </section>
        <!-- End blog list -->
<?php echo $page_footer; ?>
    </div>
<?php echo $page_footerscript; ?>
</div>
<script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=642c03d2faaa470019ff1ae6&product=inline-share-buttons&source=platform"
        async="async"></script>
<style>
    .description table {
        width: 100% !important;
        border: 1px solid #dfdfdf;
    }

    .description table tr td {
        border: 1px solid #dfdfdf;
    }

    .description {
        width: 100% !important;
        overflow: auto;
    }
</style>
</body>