<?php
echo $page_head;
$reviews = $this->md->select('tbl_review');
?>
<body id="bg" class="data-typography-1">

<div class="page-wraper">
    <?php echo $page_header; ?>

    <div class="page-content bg-white">
        <?php echo $page_breadcumb; ?>

<!--        <section class="content-inner">-->
<!--            <div class="container">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-12 intro_message mt40">-->
<!--                        --><?php
//                        if (empty($aboutus)) :
//                            echo "Sorry, content not available";
//                        else :
//                            foreach ($aboutus as $key => $aboutus_data) {
//                                echo $aboutus_data->about;
//                            }
//                        endif;
//                        ?>
<!--                    </div>-->
<!--                    <!-- End intro center -->-->
<!--                </div>-->
<!--            </div>-->
<!--        </section>-->


        <!-- About Us -->
        <section class="content-inner">
            <div class="container">
                <!-- About Services -->
                <div class="row about-bx1 align-items-center ">
                    <div class="col-lg-6">
                        <div class="dz-media p-r20">
                            <div class="image-box wow fadeInUp" data-wow-delay="0.4s">
                                <!-- <img src="assets/images/about/pic5.jpg" alt=""> -->
                                <div class="tag"><h2 class="counter">20</h2><h5>year experience</h5></div>
                                <div class="split-box h-100">
                                    <video autoplay loop muted id="about-video">
                                        <source src="assets/images/about/video.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                            <div class="split-box">
                                <img src="assets/images/about/pic6.jpg" alt="" class="wow fadeInUp" data-wow-delay="0.6s">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 about-content m-lg-t40 " >
                        <h3 class="sub-title wow fadeInUp" data-wow-delay="0.8s">WAKE UP IT’S TIME</h3>
                        <h2 class="title m-0 wow fadeInUp" data-wow-delay="1.0s">To Take The <span class="text-primary">Action</span></h2>
                        <p class="description m-b10 wow fadeInUp" data-wow-delay="1.2s">Start your training with our Professional Trainers</p>
                        <p class="wow fadeInUp" data-wow-delay="1.4s" >Nunc vulputate urna ut erat posuere accumsan. Curabitur ut commodo mauris, ac volutpat dui. Nullam eget enim ut mi bibendum ultrices. Pellentesque non feugia.</p>
                        <ul class="pr-list list-italic m-t30 m-b35 wow fadeInUp" data-wow-delay="1.6s">
                            <li><i class="flaticon-check-mark"></i>Personal Training</li>
                            <li><i class="flaticon-check-mark"></i>Body Building</li>
                            <li><i class="flaticon-check-mark"></i>Boxing Classess</li>
                            <li><i class="flaticon-check-mark"></i>Cardio And More</li>
                            <li><i class="flaticon-check-mark"></i>Personal Training</li>
                            <li><i class="flaticon-check-mark"></i>Body Building</li>
                            <li><i class="flaticon-check-mark"></i>Boxing Classess</li>
                            <li><i class="flaticon-check-mark"></i>Cardio And More</li>
                        </ul>
                        <a href="about-us.html" class="btn btn-skew btn-lg btn-primary shadow-primary wow fadeInUp" data-wow-delay="1.8s"><span class="skew-inner"><span class="text">Get Started</span></span></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Us -->

        <!-- About Us -->
        <section class=" membership-box-2 about-bx1">
            <div class="container">
                <div class="row ">
                    <div class="col-lg-5 col-md-12 wow fadeInUp about-content" data-wow-delay="0.4s">
                        <div class="about-content">
                            <h3 class="title text-white m-0">Where Passion and <span class="text-primary">Exercise Collide</span></h3>
                            <p class="description text-white m-b40">our Professional Trainers</p>
                            <ul class="nav nav-tabs style-1 light m-b20">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#tab-mission" data-bs-toggle="tab" data-bs-target="#tabMission">
                                        <span>Our Mission</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tab-vision" data-bs-toggle="tab" data-bs-target="#tabVision">
                                        <span>Our Vision</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content m-b30" id="myTabContent">
                                <div class="tab-pane fade show active" id="tabMission" role="tabpanel">
                                    <div class="content font-18 m-0">
                                        <ul class="pr-list style-1 white list-italic m-b30">
                                            <li><i class="flaticon-check-mark"></i>Personal Training</li>
                                            <li><i class="flaticon-check-mark"></i>Body Building</li>
                                            <li><i class="flaticon-check-mark"></i>Boxing Classess</li>
                                            <li><i class="flaticon-check-mark"></i>Cardio And More</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tabVision" role="tabpanel">
                                    <div class="content font-18 m-0">
                                        <ul class="pr-list style-1 white list-italic m-b30">
                                            <li><i class="flaticon-check-mark"></i>Boxing Classess</li>
                                            <li><i class="flaticon-check-mark"></i>Cardio And More</li>
                                            <li><i class="flaticon-check-mark"></i>Personal Training</li>
                                            <li><i class="flaticon-check-mark"></i>Body Building</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12">
                        <div class="dz-media">
                            <img src="assets/images/about/pic8.png" alt="" class="">
                            <h3 class="gym-text">
                                <span>F</span>
                                <span>i</span>
                                <span>t</span>
                                <span>n</span>
                                <span>e</span>
                                <span>s</span>
                                <span>s</span>
                            </h3>
                            <h3 class="gym-text-2">
                                <span>F</span>
                                <span>i</span>
                                <span>t</span>
                                <span>n</span>
                                <span>e</span>
                                <span>s</span>
                                <span>s</span>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- About Us -->


        <!-- Schedule Table -->
        <div class="content-inner">
            <div class="container">
                <div class="section-head style-2 text-center wow fadeInUp" data-wow-delay="0.4s">
                    <h2 class="title">Find The Class That Suits You</h2>
                </div>
                <div class="schedule-table style-1 table-responsive wow fadeInUp" data-wow-delay="0.6s">
                    <table class="table-responsive-md ck-table">
                        <thead>
                        <tr class="">
                            <th></th>
                            <th>Monday</th>
                            <th>Tuesday</th>
                            <th>Wednesday</th>
                            <th>Thursday</th>
                            <th>Friday</th>
                            <th>Saturday</th>
                            <th>Sunday</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="row_1">
                            <td class="event-time"> 06:00 - 07:00 </td>
                            <td class="event" rowspan="2">
                                <a class="title" href="javascript:void(0);">Open Gym</a>
                                <div class="event-tag">Gym</div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="row_2">
                            <td class="event-time"> 07:00 - 08:00 </td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Cardio Burn</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Power Lifting</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Aerobic</div>
                            </td>
                        </tr>
                        <tr class="row_3">
                            <td class="event-time"> 08:00 - 09:00 </td>
                            <td></td>
                            <td></td>
                            <td class="event" rowspan="2">
                                <a class="title" href="javascript:void(0);">Crossfit Class</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Strenght</div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="row_4">
                            <td class="event-time"> 09:00 - 10:00 </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Cardio Burn</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                        </tr>
                        <tr class="row_5">
                            <td class="event-time"> 10:00 - 11:00 </td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Crossfit Class</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Crossfit Class</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="row_6">
                            <td class="event-time"> 11:00 - 12:00 </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="row_7">
                            <td class="event-time"> 12:00 - 13:00 </td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Cardio Burn</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Power Lifting</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Aerobic</div>
                            </td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Crossfit Class</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="row_8">
                            <td class="event-time"> 13:00 - 14:00 </td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Power Lifting</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Aerobic</div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="row_9">
                            <td class="event-time"> 14:00 - 15:00 </td>
                            <td></td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Crossfit Class</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="row_10">
                            <td class="event-time"> 15:00 - 16:00 </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Cardio Burn</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="row_11">
                            <td class="event-time"> 18:00 - 19:00 </td>
                            <td></td>
                            <td class="event">
                                <a class="title" href="javascript:void(0);">Crossfit Class</a>
                                <span class="subtitle">Madison Fren</span>
                                <div class="event-tag">Hit Training</div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Schedule Table -->

        <!-- Pricing-table -->
        <section class="content-inner pt-0">
            <div class="container">
                <div class="section-head style-2 text-center wow fadeInUp" data-wow-delay="0.4s">
                    <h2 class="title">Choose Your Perfect Plan</h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 m-b30">
                        <div class="pricingtable-wrapper style-3 wow fadeInUp" data-wow-delay="0.6s">
                            <div class="pricingtable-inner">
                                <div class="pricingtable-price">
                                    <div class="pricingtable-title">Basic Plan</div>
                                    <div class="pricingtable-title premium">Popular Plan</div>
                                    <h2 class="pricingtable-bx">$10<small>/ Per User Per Month</small></h2>
                                    <p class="m-b20">Basic Feature For Up to 10 Users</p>
                                    <div class="pricingtable-button">
                                        <a href="contact-us.html" class="btn btn-dark rounded-0 d-block m-b10"><span>Get Start</span></a>
                                        <a href="contact-us.html" class="btn btn-outline-dark rounded-0 d-block"><span>Chat To Sales</span></a>
                                    </div>
                                </div>
                                <div class="pricingtable-features">
                                    <h6 class="pricingtable-sub-title">Features</h6>
                                    <p class="text">Everything in Our Free Plan Plus......</p>
                                    <ul>
                                        <li>Review Your Question</li>
                                        <li>Work with Resources </li>
                                        <li>Analysis of Your "I Have"</li>
                                        <li>Analysis of Your "I Have"</li>
                                        <li>Support & Mentoring</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 m-b30">
                        <div class="pricingtable-wrapper style-3 box-hover wow fadeInUp" data-wow-delay="0.8s">
                            <div class="pricingtable-inner">
                                <div class="pricingtable-price">
                                    <div class="pricingtable-title">Business Plan</div>
                                    <div class="pricingtable-title premium">Popular Plan</div>
                                    <h2 class="pricingtable-bx">$20<small>/ Per User Per Month</small></h2>
                                    <p class="m-b20">Growing Team Up to 20  Hours</p>
                                    <div class="pricingtable-button">
                                        <a href="contact-us.html" class="btn btn-dark rounded-0 d-block m-b10"><span>Get Start</span></a>
                                        <a href="contact-us.html" class="btn btn-outline-dark rounded-0 d-block"><span>Chat To Sales</span></a>
                                    </div>
                                </div>
                                <div class="pricingtable-features">
                                    <h6 class="pricingtable-sub-title">Features</h6>
                                    <p class="text">Everything in Our Free Plan Plus......</p>
                                    <ul>
                                        <li>Review Your Question</li>
                                        <li>Work with Resources </li>
                                        <li>Analysis of Your "I Have"</li>
                                        <li>Analysis of Your "I Have"</li>
                                        <li>Support & Mentoring</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 m-b30">
                        <div class="pricingtable-wrapper style-3 wow fadeInUp" data-wow-delay="1.0s">
                            <div class="pricingtable-inner">
                                <div class="pricingtable-price">
                                    <div class="pricingtable-title">Enterprise Plan</div>
                                    <div class="pricingtable-title premium">Popular Plan</div>
                                    <h2 class="pricingtable-bx">$90<small>/ Per User Per Month</small></h2>
                                    <p class="m-b20">Advance Features + Unlimited User</p>
                                    <div class="pricingtable-button">
                                        <a href="contact-us.html" class="btn btn-dark rounded-0 d-block m-b10"><span>Get Start</span></a>
                                        <a href="contact-us.html" class="btn btn-outline-dark rounded-0 d-block"><span>Chat To Sales</span></a>
                                    </div>
                                </div>
                                <div class="pricingtable-features">
                                    <h6 class="pricingtable-sub-title">Features</h6>
                                    <p class="text">Everything in Our Free Plan Plus......</p>
                                    <ul>
                                        <li>Review Your Question</li>
                                        <li>Work with Resources </li>
                                        <li>Analysis of Your "I Have"</li>
                                        <li>Analysis of Your "I Have"</li>
                                        <li>Support & Mentoring</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="effect"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Pricing-table -->

        <!-- Counter Section-->
        <section class="content-inner p-0 about-bx1">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 m-b30">
                                <div class="dz-team style-3 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="dz-media">
                                        <a href="team.html"><img src="assets/images/team/team-7.png" alt=""></a>
                                        <ul class="team-social">
                                            <li><a href="https://www.facebook.com/" class="fab fa-facebook-f"></a></li>
                                            <li><a href="https://www.instagram.com/" class="fab fa-instagram"></a></li>
                                            <li><a href="https://twitter.com/?lang=en" class="fab fa-twitter"></a></li>
                                        </ul>
                                    </div>
                                    <div class="dz-content ">
                                        <h4 class="dz-name">John Doe</h4>
                                        <span class="dz-position">Senior Trainer</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 m-b30">
                                <div class="dz-team style-3 wow fadeInUp" data-wow-delay="0.6s">
                                    <div class="dz-media">
                                        <a href="team.html"><img src="assets/images/team/team-8.png" alt=""></a>
                                        <ul class="team-social">
                                            <li><a href="https://www.facebook.com/" class="fab fa-facebook-f"></a></li>
                                            <li><a href="https://www.instagram.com/" class="fab fa-instagram"></a></li>
                                            <li><a href="https://twitter.com/?lang=en" class="fab fa-twitter"></a></li>
                                        </ul>
                                    </div>
                                    <div class="dz-content ">
                                        <h4 class="dz-name">Andrio</h4>
                                        <span class="dz-position">Junior Trainer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 about-content m-b40">
                        <div class="section-head style-2 m-0">
                            <h2 class="title m-0 wow fadeInUp" data-wow-delay="0.8s">They Are Always Best</h2>
                            <p class="description m-b10 wow fadeInUp" data-wow-delay="1.0s">Start your training with our Professional Trainers</p>
                            <p class="wow fadeInUp" data-wow-delay="1.2s">Lorem Ipsum available, but the majority have suffered alteration in some form, injected humour, or randomised words which don't look even slightly believable.</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-6 m-b15">
                                <div class="counter-box style-1 wow fadeInUp" data-wow-delay="1.3s">
                                    <h3 class="title"><span class="counter">872</span>+</h3>
                                    <p class="text">Members</p>
                                </div>
                            </div>
                            <div class="col-sm-4 col-6 m-b15">
                                <div class="counter-box style-1 wow fadeInUp" data-wow-delay="1.4s">
                                    <h3 class="title"><span class="counter">120</span>+</h3>
                                    <p class="text">Total Coach</p>
                                </div>
                            </div>
                            <div class="col-sm-4 col-12 m-b15">
                                <div class="counter-box style-1 wow fadeInUp" data-wow-delay="1.5s">
                                    <h3 class="title"><span class="counter">100</span>+</h3>
                                    <p class="text">Exersices Program </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- Counter Section-->

        <!-- Testimonials-->
        <section class="content-inner testimonial-wrapper1">
            <div class="container">
                <div class="section-head style-2 text-center">
                    <h2 class="title m-b5">Testimonials</h2>

                </div>
            </div>
            <div class="swiper testimonial-swiper-1">
                <div class="swiper-wrapper">
                    <?php
                    if (!empty($reviews)) {
                        foreach ($reviews as $review) {
                            ?>
                            <div class="swiper-slide wow fadeInUp" data-wow-delay="0.2s">
                                <div class="testimonial-1">
                                    <div class="testimonial-info">
                                        <i class="quotes-icon flaticon-quote"></i>
                                        <div class="testimonial-text m-sm-b20 m-b30">
                                            <p><?php echo $review->review; ?></p>
                                        </div>
                                    </div>
                                    <div class="testimonial-details">
                                        <div class="d-flex align-items-center">
                                            <div class="testimonial-pic">
                                                <img src="<?php echo base_url($review->path ? $review->path : FILENOTFOUND); ?>"
                                                     title="<?php echo $review->username; ?>"
                                                     alt="<?php echo $review->username; ?>">
                                            </div>
                                            <div class="info">
                                                <h4 class="testimonial-name"><?php echo $review->username; ?></h4>
                                                <span class="testimonial-position "><?php echo date('d-m, Y', strtotime($review->datetime)); ?></span>
                                            </div>
                                        </div>
                                        <ul class="testimonial-rating">
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
                <div class="clearfix justify-content-center d-flex m-sm-t30 m-t50">
                    <div class="num-pagination">
                        <div class="swiper-pagination dz-swiper-pagination1 style-2 justify-content-center"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Testimonials-->

    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>


<!-- end of #content -->
