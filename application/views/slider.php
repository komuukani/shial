<?php
$slider_data = $this->md->select('tbl_banner');
?>

<div class="main-bnr-one">
    <div class="swiper main-slider">
        <div class="swiper-wrapper">
            <?php
            if (!empty($slider_data)) {
                foreach ($slider_data as $slider) {
                    ?>
                    <div class="swiper-slide">
                        <div class="banner-inner"
                             style="background-image: url('<?php echo base_url($slider->path); ?>');background-size: 100% 100%">
                            <div class="container">
                                <div class="row align-items-center">
                                    <div class="col-xxl-7 col-xl-8 col-sm-8">
                                        <div class="banner-content">
                                            <div class="top-content">
                                                <div class="clearfix" data-swiper-parallax="-100">
                                                    <p class="para-1"><?php echo $slider->subtitle; ?></p>
                                                    <p class="para-2"><span><?php echo $slider->title; ?> </span></p>
                                                    <a href="<?php echo base_url('aboutus'); ?>"
                                                       class="btn btn-skew btn-lg btn-primary align-text-bottom">
														<span class="skew-inner">
															<span class="text">Get Started</span>
														</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-5 col-xl-4 col-sm-4">
                                        <div class="banner-media">

                                            <ul data-swiper-parallax="-50">
                                                <li>7 DAYS AWEEK TRAININGS</li>
                                                <li>AWESOME PROGRAMS</li>
                                                <li>FOR EVERY DAY</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
            } ?>
        </div>
        <div class="container">
            <div class="num-pagination wow bounceInUp center" data-wow-delay="1.6s">
                <div class="swiper-pagination main-pagination style-2"></div>
            </div>
        </div>
        <div class="main-btn wow wow rollIn" data-wow-delay="1.8s">
            <div class="main-btn-prev btn-prev"><i class="fa-solid fa-arrow-left"></i></div>
            <div class="main-btn-next btn-next"><i class="fa-solid fa-arrow-right"></i></div>
        </div>
    </div>
</div>


