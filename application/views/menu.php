<?php
echo $page_head;
$menutype = $this->uri->segment(2) ? $this->uri->segment(2) : ''; // Menu TYpe
$type = '';
if ($menutype == 'both') {
    $type = 'Both';
} elseif ($menutype == 'dinein') {
    $type = 'Dine In';
} elseif ($menutype == 'takeaway') {
    $type = 'Take Away';
}
$category_data = $this->md->my_query('SELECT * FROM `tbl_category` ORDER BY position')->result();
?>
<body>

<!-- Document Wrapper
    ============================================= -->
<div id="wrapper">
    <?php echo $page_breadcumb; ?>
    <?php echo $page_header; ?>
    <div id="content">
        <section class="padding-30 our_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="head_title">
                            <i class="icon-intro"></i>
                            <h1>OUR MENU</h1>
                            <span class="welcome">Choose & Taste</span>
                        </div>
                        <?php
                        if (!empty($category_data)) {
                            echo '<ul class="myMenu">';
                            foreach ($category_data as $category) {
                                echo '<li><a href="' . (current_url() . '/#category_' . $category->category_id) . '" class="text-FFF">' . $category->title . '</a></li>';
                            }
                            echo '</ul>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>

        <?php
        if (!empty($category_data)) {
            foreach ($category_data as $category) {
                ?>
                <!-- Menu Items - Break Fast
                ============================================= -->
                <?php
                if ($type) :
                    $products = $this->md->my_query('SELECT * FROM `tbl_product` WHERE (`product_type` = "' . $type . '" OR `product_type` = "Both" ) AND category_id = ' . $category->category_id . ' AND status = 1')->result();
                    if (!empty($products)) {
                        ?>
                        <section data-scroll-index="0" id="<?php echo 'category_' . $category->category_id; ?>">
                            <div class="food-banner dark">
                                <div id="menu-parallax">
                                    <div class="bcg p-0"
                                         style="background-image: url('<?php echo base_url($category->path ? $category->path : FILENOTFOUND); ?>') !important;">
                                        <!--background43 -->
                                        <div class="bg-transparent">
                                            <div class="banner-content">
                                                <div class="container"><i class="icon-breakfast"></i>
                                                    <h1><?php echo $category->title; ?></h1>
                                                </div>
                                            </div>
                                            <!-- End Banner content -->
                                        </div>
                                        <!-- End bg trnsparent -->
                                    </div>
                                </div>
                                <!-- Service parallax -->
                            </div>
                            <div class="container padding-t-40">
                                <!-- Menu Item -->
                                <?php
                                foreach ($products as $product) {
                                    ?>
                                    <article class="col-md-6 col-sm-12">
                                        <div class="menu-item-list">

                                            <!--<div class="item-img">-->
                                            <!--    <div class="overlay_content">-->
                                            <!-- Overlay Item -->
                                            <!--        <div class="overlay_item">-->
                                            <!--            <img-->
                                            <!--                src="<?php echo base_url($product->path ? $product->path : FILENOTFOUND); ?>"-->
                                            <!--                alt="<?php echo $product->title; ?>"-->
                                            <!--                title="<?php echo $product->title; ?>">-->
                                            <!--        </div>-->
                                            <!-- End Overlay Item -->
                                            <!--    </div>-->
                                            <!--</div>-->

                                            <h3><a href="javascript:void(0)"><?php echo $product->title; ?> </a>
                                                <!--                                            <span-->
                                                <!--                                                class="label red"> spicy</span> -->
                                                <span
                                                    class="price">$<?php echo $product->price; ?></span></h3>
                                            <p><?php echo $product->ingredients; ?></p>
                                        </div>
                                    </article>
                                    <?php
                                }
                                ?>
                                <!-- End Menu Item -->
                            </div>
                        </section>
                        <!-- #menu end -->
                        <?php
                    }
                endif;
            }
        }
        ?>
    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<style>
    html {
        scroll-behavior: smooth;
    }

    .myMenu {
        list-style: none;
        display: grid;
        grid-template-columns: auto auto auto auto;
    }

    .myMenu li {
        margin-bottom: 10px;
        margin-left: 5px;
        margin-right: 5px;
        background: #000 !important;
        display: inline-block;
        padding: 10px 25px;
        border-radius: 6px;
        border: 2px dashed #fff;
        text-align: center;
        text-transform: uppercase;
        letter-spacing: 1px;
    }

    .myMenu li:hover {
        background: #c59d5f !important;
    }

    .myMenu li a {
        color: #fff !important;
    }

    .menu-item-list p {
        height: 100px !important;
    }

    .menu-item-list {
        margin-bottom: 35px !important;
        box-shadow: 0 0 5px #FDFDFD;
    }

    @media only screen and (max-width: 500px) {
        .myMenu {
            display: grid;
            grid-template-columns: auto !important;
        }

        .menu-item-list {
            margin-bottom: 55px !important;
            box-shadow: 0 0 5px #FDFDFD;
        }
    }
</style>
</body>
<!-- end of #content -->
