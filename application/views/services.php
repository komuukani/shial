<?php
echo $page_head;
$reviews = $this->md->select('tbl_review');
?>
<body id="bg" class="data-typography-1">

<div class="page-wraper">
    <?php echo $page_header; ?>

    <div class="page-content bg-white">
        <?php echo $page_breadcumb; ?>

        <section class="content-inner">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.1s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-fitness"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Our Classes</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.2s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-user"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Our Trainers</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.3s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-medal"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Memberships</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.4s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-calendar"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Our Timeline</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.5s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-verify-1"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Our Trust</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.6s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-supplement-bottle"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Our Supplement</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.7s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-premium"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Premium Plan</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.8s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-training"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Weight Lifting</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="0.9s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-contact-center"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Online Support</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="1.0s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-settings-1"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Gym Services</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="1.1s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-exercise-2"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Streching</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 m-b30">
                        <div class="icon-bx-wraper style-1 hover-aware box-hover wow fadeInUp" data-wow-delay="1.2s">
                            <div class="icon-bx m-b20">
                                <div class="icon-cell text-primary">
                                    <i class="flaticon-star"></i>
                                </div>
                            </div>
                            <div class="icon-content">
                                <h4 class="dz-title m-b10"><a href="##">Best Rating</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                            
                            <div class="effect"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 

    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>


<!-- end of #content -->
