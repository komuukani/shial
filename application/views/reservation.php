<?php
echo $page_head;
?>
<body>

<!-- Document Wrapper
    ============================================= -->
<div id="wrapper">
    <?php echo $page_header; ?>
    <div id="content">
        <!-- Intro
        ============================================= -->
        <!-- RESERVATION
    ============================================= -->
        <section id="slide2-03" class="reservation dark home_2 text-center">
            <!-- Bg Parallax -->
            <div class="bcg background50"
                 data-center="background-position: 50% 0px;"
                 data-bottom-top="background-position: 50% 100px;"
                 data-top-bottom="background-position: 50% -100px;"
                 data-anchor-target="#slide2-03"
            >
                <!-- Bg Transparent -->
                <div class="bg-rgba-black-05 padding-100">
                    <div class="container">
                        <div class="row">
                            <!-- Head Title -->
                            <div class="head_title">
                                <i class="icon-intro"></i>
                                <h1>RESERVATION</h1>
                                <span class="welcome">Book your table</span>
                            </div>
                            <!-- End# Head Title -->

                            <!-- Reservation form style2-->
                            <form class="reserv_form reserv_style2" method="post">
                                <?php
                                if (isset($error)) {
                                    ?>
                                    <div class="alert alert-danger p-1">
                                        <?php echo $error; ?>
                                    </div>
                                    <?php
                                }
                                if (isset($success)) {
                                    ?>
                                    <div class="alert alert-success p-1">
                                        <?php echo $success; ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <!-- Form group -->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-sx-12">
                                                        <input name="fname" class="form-control" type="text"
                                                               placeholder="Your Name *" required>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-sx-12">
                                                        <input name="phone" class="form-control" type="tel"
                                                               placeholder="Phone No.">
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-sx-12">
                                                        <input name="email" class="form-control" type="email"
                                                               placeholder="email">
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-sx-12">
                                                        <!-- Selct wrap -->
                                                        <div class="select_wrap">
                                                            <select class="form-control" name="total_guests">
                                                                <option value="one">No. of Guests *</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10+">10+</option>
                                                            </select>
                                                        </div>
                                                        <!-- End select wrap -->
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-sx-12 datepicker">
                                                        <input name="datetime" class="form-control"
                                                               placeholder="Date" type="datetime-local">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-sx-12">
                                                        <!-- Selct wrap -->
                                                        <div class="select_wrap">
                                                            <select class="form-control" name="food">
                                                                <option value="">Preference *</option>
                                                                <option value="lunch">Lunch</option>
                                                                <option value="dinner">Dinner</option>
                                                            </select>
                                                        </div>
                                                        <!-- End select wrap -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End form group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row element">
                                    <div class="loading2"></div>
                                    <button class="btn btn-gold white" name="submit" value="Send"
                                            type="submit">BOOK YOUR TABLE
                                    </button>
                                </div>
                            </form>
                            <!-- End reserv home -->
                            <div class="done2">
                                <strong>Thank you!</strong> We have received your message.
                            </div>
                        </div>
                    </div>
                    <!-- End bg transparent -->
                </div>
                <!-- End Bg Parallax -->
        </section>
        <!-- End RESERVATION -->
    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
<!-- end of #content -->
