<?php
echo $page_head;
?>
<body>
<div id="wrapper">
    <?php echo $page_header; ?>
    <?php echo $page_breadcumb; ?>
    <div id="content">
        <!-- Testimonials
   ============================================= -->
        <section class="padding-100">
            <div class="container">
                <div class="row">
                    <!-- Article -->
                    <article>
                        <!-- Main Title -->
                        <?php
                        if (empty($terms)) :
                            echo "Sorry, content not available";
                        else :
                            foreach ($terms as $key => $terms_data) {
                                ?>
                                <div class="col-md-12 mb30">
                                    <?php echo $terms_data->terms; ?>
                                </div>
                                <!-- End# Main Title -->
                            <?php }
                        endif;
                        ?>
                    </article>
                    <!-- End# Article -->
                </div>
            </div>
        </section>
        <!-- End Testimonials -->

        <?php echo $page_footer; ?>
    </div>
    <?php echo $page_footerscript; ?>
</div>
</body>