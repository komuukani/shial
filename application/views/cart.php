<?php
echo $page_head;
$unique_id = $this->input->cookie('unique_id');
$cartData = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
$ip = $this->md->getVisIpAddr();
$ipdat = @json_decode(file_get_contents(
    "http://www.geoplugin.net/json.gp?ip=" . $ip));
$currency_fromat = (!empty($ipdat->geoplugin_countryName) && ($ipdat->geoplugin_countryName != 'India')) ? '$' : "₹";
?>
<body id="bg" class="data-typography-1">

<div class="page-wraper">
    <?php echo $page_header; ?>

    <div class="page-content bg-white">
        <?php echo $page_breadcumb; ?>
        <!-- Our Shop Cart-->
        <section class="content-inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table check-tbl wow fadeInUp" data-wow-delay="0.4s">
                                <thead>
                                <tr>
                                    <th>Package</th>
                                    <th></th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $cart_total = 0;
                                if (!empty($cartData)) {
                                    foreach ($cartData as $cartData_data) {
                                        $package_data = $this->md->select_where('tbl_package', array('package_id' => $cartData_data->package_id), 'LIMIT 1');
                                        $total = $cartData_data->price * $cartData_data->qty;
                                        $cart_total += $total;
                                        ?>
                                        <tr>
                                            <td class="product-item-img"><img
                                                    src="<?php echo base_url($package_data[0]->path ? $package_data[0]->path : FILENOTFOUND); ?>"
                                                    alt="<?php echo $package_data[0]->title; ?>"></td>
                                            <td class="product-item-name">
                                                <a href="<?php echo base_url('package'); ?>"><?php echo $package_data[0]->title; ?></a>
                                            </td>
                                            <td class="product-item-price"><?php echo $currency_fromat . '' . $cartData_data->price; ?></td>
                                            <td class="product-item-quantity">
                                                <form method="post"
                                                      class="mt-30"
                                                      id="updateQty"
                                                      action="<?php echo base_url('Pages/updateQty'); ?>"
                                                      name="updateQty">
                                                    <input type="hidden" name="cartId"
                                                           value="<?php echo $cartData_data->cart_id; ?>"/>
                                                    <div class="quantity btn-quantity style-1 me-3">
                                                        <input type="text" value="<?php echo $cartData_data->qty; ?>"
                                                               id="number"
                                                               name="demo_vertical2">
                                                    </div>
                                                    <button class="btn mt-20 btn-primary text-000" type="submit">
                                                        Update
                                                    </button>
                                                </form>
                                            </td>
                                            <td class="product-item-totle"><?php echo $currency_fromat . '' . number_format($total); ?></td>
                                            <td class="product-item-close">
                                                <a href="javascript:void(0)"
                                                   data-cartid="<?php echo $cartData_data->cart_id; ?>"
                                                   data-cart="true"
                                                   title="Remove"
                                                   class="remove-to-cart ti-close"> </a>
                                            </td>
                                        </tr>
                                    <?php }
                                } else {
                                    echo "<tr><td colspan='6' align='center' class='alert alert-danger'>Sorry, You have not added any package!</td></tr>";
                                } ?>
                                <tr>
                                    <td colspan="4" align="right">
                                        <h4 class="mt-0">Grand Total</h4>
                                    </td>
                                    <td colspan="2">
                                        <span class="ml-20 font-22 font-weight-bold text-000">
                                            <?php echo $currency_fromat . '' . $cart_total; ?>
                                        </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php
                        if (!empty($cartData)) {
                            ?>
                            <div class="row shop-form m-t30 align-items-end">
                                <div class="col-md-12 text-lg-end text-md-end text-center m-b30">
                                    <a href="<?php echo base_url('checkout'); ?>"
                                       class="btn  btn-secondary wow fadeInUp"
                                       data-wow-delay="0.8s">
                                        Checkout</a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- Our Shop Cart-->
    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<script src="assets/vendor/bootstrap-touchspin/bootstrap-touchspin.js"></script>
</body>
