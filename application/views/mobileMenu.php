<?php
$web_data = ($web_data) ? $web_data[0] : '';
?>
<!-- Offcanvas More info-->
<div class="offcanvas offcanvas-end " tabindex="-1" id="offcanvasRight">
    <div class="offcanvas-body custom-nevbar">
        <div class="row">
            <div class="col-md-7 col-xl-8">
                <div class="custom-nevbar__left">
                    <button type="button" class="close-icon d-md-none ms-auto" data-bs-dismiss="offcanvas"
                            aria-label="Close"><i class="bi bi-x"></i></button>
                    <ul class="custom-nevbar__nav mb-lg-0">
                        <li class="menu_item">
                            <a class="menu_link" href="<?php echo base_url(); ?>">Home</a>
                        </li>
                        <li class="menu_item dropdown">
                            <a class="menu_link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                               aria-expanded="false">Company</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="<?php echo base_url('aboutus'); ?>">About
                                        Company</a></li>
                                <li><a class="dropdown-item" href="<?php echo base_url('white-paper'); ?>">White
                                        Paper</a>
                                </li>
                                <li><a class="dropdown-item" href="<?php echo base_url('careers'); ?>">Career</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu_item">
                            <a class="menu_link" href="<?php echo base_url('services'); ?>">Our Services</a>
                        </li>
                        <li class="menu_item">
                            <a class="menu_link" href="<?php echo base_url('personal-loan'); ?>">Personal
                                Loan</a>
                        </li>
                        <li class="menu_item">
                            <a class="menu_link" href="<?php echo base_url('business-loan'); ?>">Business Loan</a>
                        </li>
                        <li class="menu_item">
                            <a class="menu_link" href="<?php echo base_url('contact'); ?>">Contact us</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 col-xl-4">
                <div class="custom-nevbar__right">
                    <div class="custom-nevbar__top d-none d-md-block">
                        <button type="button" class="close-icon ms-auto" data-bs-dismiss="offcanvas" aria-label="Close">
                            <i class="bi bi-x"></i></button>
                        <div class="custom-nevbar__right-thumb mb-auto">
                            <a href="<?php echo base_url(); ?>">
                                <img src="<?php echo base_url($web_data ? $web_data->logo : FILENOTFOUND); ?>"
                                     width="250px"
                                     alt="<?php echo $web_data ? $web_data->project_name : ''; ?>"
                                     title="<?php echo $web_data ? $web_data->project_name : ''; ?>">
                            </a>
                        </div>
                    </div>
                    <ul class="custom-nevbar__right-location">
                        <li>
                            <p class="mb-2">Phone: </p>
                            <a href="tel:<?php echo $web_data ? $web_data->phone : ''; ?>"
                               class="fs-4 contact"><?php echo $web_data ? $web_data->phone : ''; ?></a>
                        </li>
                        <li class="location">
                            <p class="mb-2">Email: </p>
                            <a href="mailto:<?php echo $web_data ? $web_data->email_address : ''; ?>"
                               class="fs-4 contact"><?php echo $web_data ? $web_data->email_address : ''; ?></a>
                        </li>
                        <li class="location">
                            <p class="mb-2">Location: </p>
                            <p class="fs-4 contact"><?php echo $web_data ? $web_data->address : ''; ?></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header-section end -->