<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$brands = $this->md->select_where('tbl_brand', array('status' => 1));    // get all Brand
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form name="edit" method="post" enctype="multipart/form-data">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Brand</label>
                                                                    <select name="brand_id"
                                                                            class="form-control select2 text-capitalize <?php if (form_error('brand_id')) { ?> is-invalid <?php } ?>">
                                                                        <option value="">Select Brand</option>
                                                                        <?php
                                                                        if (!empty($brands)):
                                                                            foreach ($brands as $brand):
                                                                                ?>
                                                                                <option
                                                                                    <?php echo $brand->brand_id == $updata[0]->brand_id ? 'selected' : ''; ?>
                                                                                    value="<?php echo $brand->brand_id; ?>"><?php echo ucfirst($brand->title); ?></option>
                                                                            <?php
                                                                            endforeach;
                                                                        endif;
                                                                        ?>
                                                                    </select>
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('brand_id')) {
                                                                            echo form_error('brand_id');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Title</label>
                                                                    <input type="text" name="title"
                                                                           value="<?php echo $updata[0]->title; ?>"
                                                                           placeholder="Enter Video Title"
                                                                           class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label"> Video Path/ID</label>
                                                                    <input type="text" name="video"
                                                                           value="<?php echo $updata[0]->video; ?>"
                                                                           placeholder="Enter  Video Path/ID"
                                                                           class="form-control <?php if (form_error('video')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('video')) {
                                                                            echo form_error('video');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Video
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form name="add" method="post">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Brand</label>
                                                                    <select name="brand_id"
                                                                            class="form-control select2 text-capitalize <?php if (form_error('brand_id')) { ?> is-invalid <?php } ?>">
                                                                        <option value="">Select Brand</option>
                                                                        <?php
                                                                        if (!empty($brands)):
                                                                            foreach ($brands as $brand):
                                                                                ?>
                                                                                <option
                                                                                    value="<?php echo $brand->brand_id; ?>"><?php echo ucfirst($brand->title); ?></option>
                                                                            <?php
                                                                            endforeach;
                                                                        endif;
                                                                        ?>
                                                                    </select>
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('brand_id')) {
                                                                            echo form_error('brand_id');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Title</label>
                                                                    <input type="text" name="title" value="<?php
                                                                    if (set_value('title') && !isset($success)) {
                                                                        echo set_value('title');
                                                                    }
                                                                    ?>" placeholder="Enter Video Title"
                                                                           class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label"> Video Path/ID</label>
                                                                    <input type="text" name="video" value="<?php
                                                                    if (set_value('video') && !isset($success)) {
                                                                        echo set_value('video');
                                                                    }
                                                                    ?>" placeholder="Enter Video Path/ID"
                                                                           class="form-control <?php if (form_error('video')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('video')) {
                                                                            echo form_error('video');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Upload Video
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="videoTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Brand</th>
                                                        <th>Title</th>
                                                        <th>Video</th>
                                                        <th>Entry Date</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['status']):
                                                            echo '<th>Action</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'status'},
        {data: 'brand'},
        {data: 'title'},
        {data: 'video'},
        {data: 'datetime'},
        <?php
        if ($permission['all'] || $permission['status']):
            echo '{data: "action"},';
        endif;
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
</script>
</body>   