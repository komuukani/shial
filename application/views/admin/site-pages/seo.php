<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <div class="section-body">
                        <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?> 
                        <div class="row">
                            <?php
                            if ($page_type == "add" || $page_type == "edit"):
                                ?>
                                <!-- >> ADD/EDIT Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">  
                                            <?php
                                            if (isset($updata)):
                                                if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                    ?>
                                                    <!-- >> Edit Form Start
                                                    ================================================== --> 
                                                    <form name="update" method="post">
                                                        <div class="panel-body"> 
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Select Page </label>
                                                                        <select name="page" class="form-control select2 <?php if (form_error('page')) { ?> is-invalid <?php } ?> text-capitalize">
                                                                            <option <?php echo ($updata[0]->page == 'index') ? 'selected' : ''; ?> >index</option>
                                                                            <?php
                                                                            $fields = $this->db->list_fields('tbl_control');
                                                                            foreach ($fields as $fields_val):
                                                                                if ($fields_val != 'control_id'):
                                                                                    ?>
                                                                                    <option <?php echo ($updata[0]->page == $fields_val) ? 'selected' : ''; ?>><?php echo $fields_val; ?></option>
                                                                                    <?php
                                                                                endif;
                                                                            endforeach;
                                                                            $category = $this->md->select('tbl_category');
                                                                            foreach ($category as $category_val):
                                                                                ?>
                                                                                <option value="<?php echo $category_val->category_id; ?>"><?php echo $category_val->title; ?></option>
                                                                                <?php
                                                                            endforeach;
                                                                            ?>  
                                                                        </select>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('page')) {
                                                                                echo form_error('page');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Meta Title <small class="text-danger">*Meta title maximum length is 70 </small></label>
                                                                        <input type="text" name="meta_title" value="<?php echo $updata[0]->title; ?>" placeholder="Enter Meta Title" class="form-control <?php if (form_error('meta_title')) { ?> is-invalid <?php } ?>">
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('meta_title')) {
                                                                                echo form_error('meta_title');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>   
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Enter Meta keywords <small class="text-danger">*Meta keywords allow maximum 30 words </small></label> 
                                                                        <textarea class="form-control <?php if (form_error('meta_keyword')) { ?> is-invalid <?php } ?>" placeholder="Enter Meta Keywords" style="height: 140px" name="meta_keyword"><?php echo $updata[0]->keyword; ?></textarea>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('meta_keyword')) {
                                                                                echo form_error('meta_keyword');
                                                                            }
                                                                            ?>
                                                                        </div> 
                                                                    </div>   
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Enter Meta Description <small class="text-danger">*Meta description length is between 150 to 160 </small> </label> 
                                                                        <textarea class="form-control <?php if (form_error('meta_desc')) { ?> is-invalid <?php } ?>" placeholder="Enter Meta Description" style="height: 140px" name="meta_desc"><?php echo $updata[0]->description; ?></textarea>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('meta_desc')) {
                                                                                echo form_error('meta_desc');
                                                                            }
                                                                            ?>
                                                                        </div> 
                                                                    </div>   
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Upload SEO</button>
                                                            <a href="<?php echo base_url($current_page . '/show'); ?>" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" >Cancel</a> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Edit Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            else:
                                                if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                    ?>  
                                                    <!-- >> Add Form Start
                                                       ================================================== -->
                                                    <form name="add" method="post">
                                                        <div class="panel-body"> 
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Select Page </label>
                                                                        <select name="page" class="form-control select2 <?php if (form_error('page')) { ?> is-invalid <?php } ?> text-capitalize">
                                                                            <option>index</option>
                                                                            <?php
                                                                            $fields = $this->db->list_fields('tbl_control');
                                                                            foreach ($fields as $fields_val):
                                                                                if ($fields_val != 'control_id'):
                                                                                    ?>
                                                                                    <option><?php echo $fields_val; ?></option>
                                                                                    <?php
                                                                                endif;
                                                                            endforeach;
                                                                            $category = $this->md->select('tbl_category');
                                                                            foreach ($category as $category_val):
                                                                                ?>
                                                                                <option value="<?php echo $category_val->category_id; ?>"><?php echo $category_val->title; ?></option>
                                                                                <?php
                                                                            endforeach;
                                                                            ?>  
                                                                        </select>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('page')) {
                                                                                echo form_error('page');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Meta Title <small class="text-danger">*Meta title maximum length is 70 </small></label>
                                                                        <input type="text" name="meta_title" value="<?php
                                                                        if (set_value('meta_title') && !isset($success)) {
                                                                            echo set_value('meta_title');
                                                                        }
                                                                        ?>" placeholder="Enter Meta Title" class="form-control <?php if (form_error('meta_title')) { ?> is-invalid <?php } ?>">
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('meta_title')) {
                                                                                echo form_error('meta_title');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>   
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Enter Meta keywords <small class="text-danger">*Meta keywords allow maximum 30 words </small></label> 
                                                                        <textarea class="form-control <?php if (form_error('meta_keyword')) { ?> is-invalid <?php } ?>" placeholder="Enter Meta Keywords" style="height: 140px" name="meta_keyword"></textarea>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('meta_keyword')) {
                                                                                echo form_error('meta_keyword');
                                                                            }
                                                                            ?>
                                                                        </div> 
                                                                    </div>   
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Enter Meta Description <small class="text-danger">*Meta description length is between 150 to 160 </small> </label> 
                                                                        <textarea class="form-control <?php if (form_error('meta_desc')) { ?> is-invalid <?php } ?>" placeholder="Enter Meta Description" style="height: 140px" name="meta_desc"></textarea>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('meta_desc')) {
                                                                                echo form_error('meta_desc');
                                                                            }
                                                                            ?>
                                                                        </div> 
                                                                    </div>   
                                                                </div>
                                                            </div> 
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="add">Upload SEO</button>
                                                            <button class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" type="reset">Reset Form</button> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Add Form End
                                                       ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            endif;
                                            ?> 
                                        </div>
                                    </div> 
                                </div>
                                <!-- << ADD/EDIT Data END
                                ================================================== -->
                                <?php
                            else:
                                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                    ?> 
                                    <!-- >> Table Data Start
                                    ================================================== -->
                                    <div class="col-md-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"> 
                                                <div class="table-responsive">
                                                    <table class="table mb-none table-hover" id="seoTable">
                                                        <thead>
                                                            <tr> 
                                                                <th>Page</th>
                                                                <th>Title</th>
                                                                <th>Keyword</th>
                                                                <th>Description</th>
                                                                <?php
                                                                if ($permission['all'] || $permission['edit']):
                                                                    echo '<th>Edit</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>  
                                                            </tr>
                                                        </thead> 
                                                    </table>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- << Table Data End
                                    ================================================== -->
                                    <?php
                                else:
                                    $this->load->view('admin/common/access_denied');
                                endif;
                            endif;
                            ?>
                        </div> 
                    </div>
                </section> 
            </div>
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer  ?> 
        </div>
    </div>  
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'page'},
            {data: 'title'},
            {data: 'description'},
            {data: 'keyword'},
<?php
if ($permission['all'] || $permission['edit']):
    echo '{data: "edit"},';
endif;
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE] 
        getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
    </script>
</body>    
