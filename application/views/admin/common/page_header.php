<div class="row">
    <div class="col-md-6">
        <h2 class="section-title mt-0"><?php echo ucfirst($page_type) . " " . ucfirst($active_page) . ' Data'; ?></h2> 
    </div>
    <div class="col-md-6 text-right">
        <?php
        if ($permission['all'] || $permission['write']):  // If user has write/add permission
            ?>
            <a href="<?php echo base_url($current_page . '/' . (($page_type == 'show' || $page_type == '') ? 'add' : 'show')); ?>" class="btn btn-primary btn-sm"><?php echo (($page_type == 'show' || $page_type == '') ? 'Add ' . ucfirst($active_page) : 'Show ' . ucfirst($active_page)); ?></a>
            <?php
        endif;
        ?> 
    </div>
</div>