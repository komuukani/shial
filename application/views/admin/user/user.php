<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$control_data = $this->md->select_where('tbl_control', array('control_id' => 1));
$fields = $this->md->select('tbl_role');    // get all role
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <div class="section-body">
                        <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>  
                        <div class="row">
                            <?php
                            if ($page_type == "add" || $page_type == "edit"):
                                ?>
                                <!-- >> ADD/EDIT Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body"> 
                                            <?php
                                            if (isset($updata)):
                                                if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                    ?>
                                                    <!-- >> Edit Form Start
                                                    ================================================== --> 
                                                    <form method="post" name="edit" enctype="multipart/form-data">
                                                        <div class="panel-body row">
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">User Fullname</label>
                                                                <input type="text" name="username" value="<?php echo $updata[0]->admin_name; ?>" placeholder="Enter User Fullname" class="form-control <?php if (form_error('username')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('username')) {
                                                                        echo form_error('username');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Email</label>
                                                                <input type="email" name="email" readonly="" value="<?php echo $updata[0]->email; ?>" placeholder="Enter Email" class="form-control <?php if (form_error('email')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('email')) {
                                                                        echo form_error('email');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4 pass_hide_show">
                                                                <label class="control-label">Password</label>
                                                                <input type="password" name="password" value="<?php echo $this->encryption->decrypt($updata[0]->password); ?>" placeholder="Enter Password" class="form-control <?php if (form_error('password')) { ?> is-invalid <?php } ?>">
                                                                <div class="action-btn" style="width: auto">
                                                                    <i class="fa fa-eye" title="Show Password"></i>
                                                                </div>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('password')) {
                                                                        echo form_error('password');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Phone</label>
                                                                <input type="tel" name="phone" value="<?php echo $updata[0]->phone; ?>" placeholder="Enter Phone" maxlength="10" class="form-control <?php if (form_error('phone')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('phone')) {
                                                                        echo form_error('phone');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">User Role</label> &nbsp; <a href="<?php echo base_url('manage-role/add'); ?>" class="btn btn-link">Add New Role  <i class="fas fa-chevron-right"></i></a>
                                                                <select name="role" class="form-control select2 text-capitalize <?php if (form_error('role')) { ?> is-invalid <?php } ?>">
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option <?php echo ($fields_val->role_id == $updata[0]->user_role_id) ? 'selected' : ''; ?> value="<?php echo $fields_val->role_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                            <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>  
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('role')) {
                                                                        echo form_error('role');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Profile Status</label> <Br/>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="active_profile" value="1" name="status" <?php echo ($updata[0]->status) ? 'checked' : '' ?> class="custom-control-input">
                                                                    <label class="custom-control-label" for="active_profile">Active</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="inactive_profile" value="0" name="status" <?php echo (!$updata[0]->status) ? 'checked' : '' ?> class="custom-control-input">
                                                                    <label class="custom-control-label" for="inactive_profile">Inactive</label>
                                                                </div> 
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('status')) {
                                                                        echo form_error('status');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">User Address</label>
                                                                <textarea style="height: 100px" name="address" class="form-control <?php if (form_error('address')) { ?> is-invalid <?php } ?>" placeholder="Enter User Address"><?php echo $updata[0]->address; ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('address')) {
                                                                        echo form_error('address');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">Select Profile Photo <span style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span> </label>
                                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="input-append"> 
                                                                        <input type="file" id="file" onchange="readURL(this, 'blah');$('#updateStatus').val('yes');" name="user" class="" accept="image/*">  
                                                                        <input type="hidden" id="updateStatus" name="updateStatus" />
                                                                        <input type="hidden" value="<?php echo $updata[0]->path; ?>" name="oldPath" />
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if ($updata[0]->path):
                                                                    ?>
                                                                    <img src="<?php echo base_url($updata[0]->path); ?>" class="mt-20 center-block" width="50" id="blah" />
                                                                    <?php
                                                                else:
                                                                    ?>
                                                                    <img class="mt-20 center-block" width="50" id="blah" />
                                                                <?php
                                                                endif;
                                                                ?> 
                                                                <p class="error-text file-error" style="display: none">Select a valid file!</p> 
                                                            </div>    
                                                        </div> 
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Update User</button>
                                                            <a href="<?php echo base_url($current_page . '/show'); ?>" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" >Cancel</a> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Edit Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            else:
                                                if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                    ?>  
                                                    <!-- >> Add Form Start
                                                    ================================================== -->
                                                    <form method="post" name="add" enctype="multipart/form-data">
                                                        <div class="panel-body row">
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">User Fullname</label>
                                                                <input type="text" name="username" value="<?php
                                                                if (set_value('username') && !isset($success)) {
                                                                    echo set_value('username');
                                                                }
                                                                ?>" placeholder="Enter User Fullname" class="form-control <?php if (form_error('username')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('username')) {
                                                                        echo form_error('username');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Email</label>
                                                                <input type="email" name="email" value="<?php
                                                                if (set_value('email') && !isset($success)) {
                                                                    echo set_value('email');
                                                                }
                                                                ?>" placeholder="Enter Email" class="form-control <?php if (form_error('email')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('email')) {
                                                                        echo form_error('email');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4 pass_hide_show">
                                                                <label class="control-label">Password</label>
                                                                <input type="password" name="password" value="<?php
                                                                if (set_value('password') && !isset($success)) {
                                                                    echo set_value('password');
                                                                }
                                                                ?>" placeholder="Enter Password" class="form-control <?php if (form_error('password')) { ?> is-invalid <?php } ?>">
                                                                <div class="action-btn" style="width: auto">
                                                                    <i class="fa fa-eye" title="Show Password"></i>
                                                                </div>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('password')) {
                                                                        echo form_error('password');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Phone</label>
                                                                <input type="tel" name="phone" value="<?php
                                                                if (set_value('phone') && !isset($success)) {
                                                                    echo set_value('phone');
                                                                }
                                                                ?>" placeholder="Enter Phone" maxlength="10" class="form-control <?php if (form_error('phone')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('phone')) {
                                                                        echo form_error('phone');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">User Role</label> &nbsp; <a href="<?php echo base_url('manage-role/add'); ?>" class="btn btn-link">Add New Role  <i class="fas fa-chevron-right"></i></a>
                                                                <select name="role" class="form-control select2 text-capitalize <?php if (form_error('role')) { ?> is-invalid <?php } ?>">
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            echo '<option value="' . $fields_val->role_id . '">' . $fields_val->title . '</option>';
                                                                        endforeach;
                                                                    endif;
                                                                    ?>  
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('role')) {
                                                                        echo form_error('role');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Profile Status</label> <Br/>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="active_profile" value="1" name="status" checked="" class="custom-control-input">
                                                                    <label class="custom-control-label" for="active_profile">Active</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="inactive_profile" value="0" name="status" class="custom-control-input">
                                                                    <label class="custom-control-label" for="inactive_profile">Inactive</label>
                                                                </div> 
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('status')) {
                                                                        echo form_error('status');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">User Address</label>
                                                                <textarea style="height: 100px" name="address" class="form-control <?php if (form_error('address')) { ?> is-invalid <?php } ?>" placeholder="Enter User Address"><?php
                                                                    if (set_value('address') && !isset($success)) {
                                                                        echo set_value('address');
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('address')) {
                                                                        echo form_error('address');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">Select User Photo <span style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span> </label>
                                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="input-append"> 
                                                                        <input type="file" id="file" onchange="readURL(this, 'blah');" name="user" class="" accept="image/*">  
                                                                    </div>
                                                                </div>
                                                                <img src="" class="mt-20 center-block" width="40" id="blah" />
                                                                <p class="error-text file-error" style="display: none">Select a valid file!</p> 
                                                            </div> 
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="add">Add User</button>
                                                            <button class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" type="reset">Reset Form</button> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Add Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            endif;
                                            ?> 
                                        </div>
                                    </div> 
                                </div>
                                <!-- << ADD/EDIT Data END
                                ================================================== -->
                                <?php
                            else:
                                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                    ?> 
                                    <!-- >> Table Data Start
                                    ================================================== -->
                                    <div class="col-md-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"> 
                                                <div class="">
                                                    <table class="table mb-none table-hover" id="adminTable">
                                                        <thead>
                                                            <tr> 
                                                                <th>Status</th> 
                                                                <th>Role</th> 
                                                                <th>Username</th> 
                                                                <th>Contact</th>  
                                                                <th class="none">Address</th> 
                                                                <th>Photo</th> 
                                                                <th class="none">Join Date</th>  
                                                                <?php
                                                                if ($permission['all'] || $permission['status']):
                                                                    echo '<th>Action</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['edit']):
                                                                    echo '<th>Edit</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>  
                                                            </tr>
                                                        </thead> 
                                                    </table>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- << Table Data End
                                    ================================================== -->
                                    <?php
                                else:
                                    $this->load->view('admin/common/access_denied');
                                endif;
                            endif;
                            ?>
                        </div> 
                    </div>
                </section> 
            </div>
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer  ?> 
        </div>
    </div>  
    <!-- >> Permission Modal Start
    ================================================== -->
    <div class="modal fade" tabindex="-1" role="dialog" id="assign_permission">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Assign Permission to User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" name="assign_permission_form" id="assign_permission_form">
                        <div class="table-responsive" id="permission_area"> 

                        </div>
                    </form>
                    <hr />
                    <a href="<?php echo base_url('manage-permission/add'); ?>" target="_blank" class="btn btn-link">Add More Permission <i class="fas fa-plus-circle font-11"></i> </a>
                </div>
                <div class="modal-footer bg-whitesmoke br">  
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="assign" value="assign" class="btn btn-primary" form="assign_permission_form">Save changes</button> 
                </div>
            </div>
        </div>
    </div>
    <!-- << Permission Modal End
    ================================================== -->
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'status'},
            {data: 'user_role_id'},
            {data: 'admin_name'},
            {data: 'contact'},
            {data: 'address'},
            {data: 'path'},
            {data: 'datetime'},
<?php
if ($permission['all'] || $permission['status']):
    echo '{data: "action"},';
endif;
if ($permission['all'] || $permission['edit']):
    echo '{data: "edit"},';
endif;
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE, ENABLE_EXPAND_MODE(+) ]
        getDataTable('<?php echo 'admin'; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'path', true);
    </script>
</body>  