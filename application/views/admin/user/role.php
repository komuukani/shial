<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <div class="section-body">
                        <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?> 
                        <div class="row">
                            <?php
                            if ($page_type == "add" || $page_type == "edit"):
                                ?>
                                <!-- >> ADD/EDIT Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body"> 
                                            <?php
                                            if (isset($updata)):
                                                if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                    ?>
                                                    <!-- >> Edit Form Start
                                                    ================================================== --> 
                                                    <form name="update" method="post"> 
                                                        <div class="panel-body row">  
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Title</label>
                                                                <input type="text" name="title" value="<?php echo $updata[0]->title; ?>" placeholder="Enter User Role" class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('title')) {
                                                                        echo form_error('title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group col-md-8">
                                                                <label class="control-label">Remark</label>
                                                                <input type="text" name="remark" value="<?php echo $updata[0]->remark; ?>" placeholder="Enter Remark" class="form-control <?php if (form_error('remark')) { ?> is-invalid <?php } ?>"> 
                                                            </div> 
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Update User Role</button>
                                                            <a href="<?php echo base_url($current_page . '/show'); ?>" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" >Cancel</a> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Edit Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            else:
                                                if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                    ?>  
                                                    <!-- >> Add Form Start
                                                     ================================================== -->
                                                    <form name="add" method="post">
                                                        <div class="panel-body row">  
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label"><code>*</code>Title</label>
                                                                <input type="text" name="title" value="<?php
                                                                if (set_value('title') && !isset($success)) {
                                                                    echo set_value('title');
                                                                }
                                                                ?>" placeholder="Enter User Role Title" class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('title')) {
                                                                        echo form_error('title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>     
                                                            <div class="form-group col-md-8">
                                                                <label class="control-label">Remark</label>
                                                                <input type="text" name="remark" value="<?php
                                                                if (set_value('remark') && !isset($success)) {
                                                                    echo set_value('remark');
                                                                }
                                                                ?>" placeholder="Enter Remark" class="form-control <?php if (form_error('remark')) { ?> is-invalid <?php } ?>"> 
                                                            </div>     
                                                        </div>
                                                        <footer class="panel-footer">
                                                            <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="add">Add User Role</button>
                                                            <button class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" type="reset">Reset Form</button> 
                                                        </footer>
                                                    </form>
                                                    <!-- << Add Form End
                                                    ================================================== -->
                                                    <?php
                                                else:
                                                    $this->load->view('admin/common/access_denied');
                                                endif;
                                            endif;
                                            ?> 
                                        </div>
                                    </div> 
                                </div>
                                 <!-- << ADD/EDIT Data END
                                ================================================== -->
                                <?php
                            else:
                                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                    ?> 
                                    <!-- >> Table Data Start
                                    ================================================== -->
                                    <div class="col-md-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"> 
                                                <div class="">
                                                    <table class="table mb-none table-hover" id="roleTable">
                                                        <thead>
                                                            <tr>  
                                                                <th>Title</th>  
                                                                <th>Permission</th> 
                                                                <th>Remark</th>  
                                                                <?php
                                                                if ($permission['all'] || $permission['edit']):
                                                                    echo '<th>Edit</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>  
                                                            </tr>
                                                        </thead> 
                                                    </table>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- << Table Data End
                                    ================================================== -->
                                    <?php
                                else:
                                    $this->load->view('admin/common/access_denied');
                                endif;
                            endif;
                            ?>
                        </div>
                    </div> 
                </section>
            </div> 
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer  ?> 
        </div>
    </div> 
    <!-- >> Permission Modal Start
    ================================================== -->
    <div class="modal fade" tabindex="-1" role="dialog" id="assign_permission">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Assign Permission to User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" name="assign_permission_form" id="assign_permission_form">
                        <div class="table-responsive" id="permission_area"> 

                        </div>
                    </form>
                    <hr />
                    <a href="<?php echo base_url('manage-permission/add'); ?>" target="_blank" class="btn btn-link">Add More Permission <i class="fas fa-plus-circle font-11"></i> </a>
                </div>
                <div class="modal-footer bg-whitesmoke br">  
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="assign" value="assign" class="btn btn-primary" form="assign_permission_form">Save changes</button> 
                </div>
            </div>
        </div>
    </div>
    <!-- << Permission Modal End
    ================================================== -->
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'title'},
            {data: 'permission'},
            {data: 'remark'},
<?php
if ($permission['all'] || $permission['edit']):
    echo '{data: "edit"},';
endif;
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE] 
        getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
    </script>
</body>