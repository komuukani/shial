<?php
echo $page_head;    //  Load Head Link and Scripts
$control_data = $this->md->select_where('tbl_control', array('control_id' => 1));
$control_feature_data = $this->md->select_where('tbl_control_feature', array('control_feature_id' => 1));
$fields = $this->db->list_fields('tbl_control');
$field = $this->db->list_fields('tbl_control_feature');
?>
<body> 
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info alert-has-icon p-4">
                                <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                                <div class="alert-body">
                                    <div class="alert-title">Attention!</div> 
                                    <p>Hey user, you can hide and show of any pages by enable/disable of page name. In setting page there features, these features will also hide and show by this page. You can hide and show of pages and features as per your need.</p> 
                                </div>
                            </div> 
                            <div class="main-card mb-3 card">
                                <div class="card-body">  
                                    <h5 class="card-title">Enable/Disable - Page</h5>
                                    <!-- <label class="custom-switch mt-2">
                                        <input type="checkbox" 
                                               id="check_all" 
                                               onchange="checkAllPages()" 
                                               name="check_all" 
                                               class="custom-switch-input">
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">Check All</span>
                                    </label> -->
                                    <hr />
                                    <form method="post"> 
                                        <div class="panel-body" id="radio-pages">  
                                            <div class="row"> 
                                                <?php
                                                foreach ($fields as $fields_val):
                                                    if ($fields_val != 'control_id'):
                                                        ?>
                                                        <div class="col-md-3"> 
                                                            <label class="custom-switch mt-2">
                                                                <input type="checkbox" 
                                                                       id="check<?php echo ucfirst($fields_val); ?>" 
                                                                       <?php
                                                                       if (WEBSITE_STATUS == "live"):
                                                                           ?>
                                                                           onchange="getAction(this.name, this.id, 'page');"
                                                                           <?php
                                                                       else:
                                                                           ?>
                                                                           onchange="alert('Sorry, action does not perfom in demo mode!');"
                                                                       <?php
                                                                       endif;
                                                                       ?> 
                                                                       name ="<?php echo $fields_val; ?>" 
                                                                       class="custom-switch-input"
                                                                       <?php echo ($control_data[0]->$fields_val) ? 'checked' : ''; ?> >
                                                                <span class="custom-switch-indicator"></span>
                                                                <span class="custom-switch-description"><?php echo ucfirst(str_replace("_", " ", $fields_val)); ?></span>
                                                            </label> 
                                                        </div>
                                                        <?php
                                                    endif;
                                                endforeach;
                                                ?>   
                                            </div> 
                                        </div> 
                                    </form> 
                                    <hr />
                                    <h5 class="card-title">Enable/Disable - Setting Features</h5>
                                    <hr />
                                    <form method="post"> 
                                        <div class="panel-body"> 
                                            <div class="row"> 
                                                <?php
                                                foreach ($field as $field_val):
                                                    if ($field_val != 'control_feature_id'):
                                                        ?>
                                                        <div class="col-md-3"> 
                                                            <label class="custom-switch mt-2">
                                                                <input type="checkbox" 
                                                                       id="check<?php echo ucfirst($field_val); ?>" 
                                                                       onchange="getAction(this.name, this.id, 'feature');" 
                                                                       name="<?php echo $field_val; ?>" 
                                                                       class="custom-switch-input"
                                                                       <?php echo ($control_feature_data[0]->$field_val) ? 'checked' : ''; ?> >
                                                                <span class="custom-switch-indicator"></span>
                                                                <span class="custom-switch-description"><?php echo ucfirst($field_val); ?></span>
                                                            </label> 
                                                        </div>
                                                        <?php
                                                    endif;
                                                endforeach;
                                                ?>   
                                            </div> 
                                        </div> 
                                    </form> 
                                </div>
                            </div> 
                        </div> 
                    </div> 
                </section>
            </div> 
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer  ?> 
        </div>
    </div> 
    <?php echo $page_footerscript; // Load Footer script?>   
</body> 
