<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$slots = $this->md->my_query('SELECT * FROM `tbl_slot` WHERE `date` >= CURDATE() GROUP by `date`')->result();
$web_data = ($web_data) ? $web_data[0] : '';
?>
<body id="bg" class="data-typography-1">

<div class="page-wraper">
    <?php echo $page_header; ?>

    <div class="page-content bg-000 pb-100">

        <!--banner-->
        <div class="contact-bnr" style="background-image: url(assets/images/background/bg9.png);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">

                        <div class="contact-info style-1 text-start text-white">
                            <h2 class="title wow fadeInUp" data-wow-delay="0.1s">FREE CONSULTANCY</h2>
                            <p class="text wow fadeInUp" data-wow-delay="0.2s"><span
                                    class="text-decoration-underline text-white"><a class="text-white"
                                                                                    href="javascript:void(0)">We are here to help you;</a></span>
                                <br> Our experts are available to answer any questions you might have. We’ve got the
                                answers.</p>
                            <div class="contact-bottom wow fadeInUp" data-wow-delay="0.3s">
                                <div class="contact-left">
                                    <h3>Call Us</h3>
                                    <ul>
                                        <li>
                                            <a href="tel:<?php echo($admin_data->phone); ?>"><?php echo($admin_data->phone); ?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="contact-right">
                                    <h3>Email Us</h3>
                                    <ul>
                                        <li>
                                            <a href="mailto:<?php echo($admin_data->email_address); ?>"><?php echo($admin_data->email_address); ?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="contact-left mt-30">
                                    <h3>Address</h3>
                                    <ul>
                                        <li><?php echo($admin_data->address); ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-area1 style-1 m-r20 m-md-r0">
                            <form class="dz-form" method="post">
                                <?php
                                if (isset($error)) {
                                    ?>
                                    <div class="alert alert-danger p-1">
                                        <?php echo $error; ?>
                                    </div>
                                    <?php
                                }
                                if (isset($success)) {
                                    ?>
                                    <div class="alert alert-success p-1">
                                        <?php echo $success; ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div>
                                    <label class="form-label">Your Name</label>
                                    <div class="input-group">
                                        <input required type="text" value="<?php
                                        if (set_value('fname') && !isset($success)) {
                                            echo set_value('fname');
                                        }
                                        ?>" class="form-control" name="fname">
                                    </div>
                                    <div class="error-text">
                                        <?php
                                        if (form_error('fname')) {
                                            echo form_error('fname');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div>
                                    <label class="form-label">Email Address</label>
                                    <div class="input-group">
                                        <input required type="email" value="<?php
                                        if (set_value('email') && !isset($success)) {
                                            echo set_value('email');
                                        }
                                        ?>" class="form-control" name="email">
                                    </div>
                                    <div class="error-text">
                                        <?php
                                        if (form_error('email')) {
                                            echo form_error('email');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div>
                                    <label class="form-label">Phone Number</label>
                                    <div class="input-group">
                                        <input required type="text" value="<?php
                                        if (set_value('phone') && !isset($success)) {
                                            echo set_value('phone');
                                        }
                                        ?>" class="form-control" name="phone">
                                    </div>
                                    <div class="error-text">
                                        <?php
                                        if (form_error('phone')) {
                                            echo form_error('phone');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div>
                                    <label class="form-label">Select Slots</label>
                                    <div class="input-group">
                                        <select class="form-control" id="slotDate" name="slotDate">
                                            <option value="">Select Slot</option>
                                            <?php
                                            if ($slots) {
                                                foreach ($slots as $slot) {
                                                    echo '<option value="' . $slot->date . '">' . date('d-M-Y', strtotime($slot->date)) . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="error-text">
                                        <?php
                                        if (form_error('slotDate')) {
                                            echo form_error('slotDate');
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div>
                                    <label class="form-label">Select Slots Time</label>
                                    <div class="input-group">
                                        <select class="form-control" id="slot_time" name="slot_time">
                                            <option value="">Select Slot Time</option>
                                        </select>
                                    </div>
                                    <div class="error-text">
                                        <?php
                                        if (form_error('slot_time')) {
                                            echo form_error('slot_time');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div>
                                    <label class="form-label">Message</label>
                                    <div class="input-group m-b30">
                                    <textarea name="message" rows="4" required class="form-control m-b10"><?php
                                        if (set_value('message') && !isset($success)) {
                                            echo set_value('message');
                                        }
                                        ?></textarea>
                                    </div>
                                    <div class="error-text">
                                        <?php
                                        if (form_error('message')) {
                                            echo form_error('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                if ($admin_data->captcha_visibility) :
                                    echo '<div class="g-recaptcha" data-sitekey="' . $admin_data->captcha_site_key . '"></div>';
                                endif;
                                ?>
                                <div class="mt-20">
                                    <button name="submit" type="submit" value="submit"
                                            class="btn btn-primary w-100 btnhover">SUBMIT
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>