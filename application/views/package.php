<?php
echo $page_head;
$package = $this->md->select('tbl_package');
$ip =  $this->md->getVisIpAddr();
$ipdat = @json_decode(file_get_contents(
    "http://www.geoplugin.net/json.gp?ip=" . $ip));
?>

<body id="bg" class="data-typography-1">

<div class="page-wraper">
    <?php echo $page_header; ?>

    <div class="page-content bg-white">
        <?php echo $page_breadcumb; ?>

        <!-- Our Team -->
        <section class="content-inner">
            <div class="container">
                <div class="section-head style-2 text-center wow fadeInUp" data-wow-delay="0.4s">
                    <h2 class="title">Choose Your Perfect Package</h2>
                </div>
                <div class="row justify-content-center">
                    <?php
                    if (!empty($package)) {
                        foreach ($package as $package_data) {
                            $url = base_url('package/' . strtolower($package_data->slug));
                            $currency_fromat = (!empty($ipdat->geoplugin_countryName) && ($ipdat->geoplugin_countryName != 'India')) ? '$' : "₹";
                            ?>
                            <div class="col-lg-4 col-md-6 m-b30">
                                <div class="pricingtable-wrapper style-3 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="pricingtable-inner">
                                        <div class="pricingtable-price">
                                            <div class="pricingtable-title"><?php echo $package_data->title; ?></div>
                                            <h2 class="pricingtable-bx"><?php echo $currency_fromat . '' . $package_data->indian_price; ?>
                                                <small>/ <?php echo $package_data->duration; ?></small></h2>

                                            <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>">
                                                <?php
                                                $cartData = $this->md->select_where('tbl_cart', array('package_id' => $package_data->package_id, 'unique_id' => $this->input->cookie('unique_id')));
                                                ?>
                                                <input type="hidden" name="package_id"
                                                       value="<?php echo $package_data->package_id; ?>">
                                                <input type="hidden" name="price"
                                                       value="<?php echo (!empty($ipdat->geoplugin_countryName) && ($ipdat->geoplugin_countryName != 'India')) ? $package_data->overseas_price : $package_data->indian_price; ?>">
                                                <input type="hidden" name="qty"
                                                       value="<?php echo $cartData ? ($cartData[0]->qty + 1) : 1 ?>">

                                                <div class="pricingtable-button">
                                                    <button type="submit" value="send"
                                                            class="btn btn-dark rounded-0 d-block m-b10"><span>Add to cart</span>
                                                    </button>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="pricingtable-features">
                                            <h6 class="pricingtable-sub-title">Features</h6>
                                            <?php echo $package_data->description; ?>
                                        </div>
                                    </div>
                                    <div class="effect"></div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </section>
        <!-- Our Team -->

        <!-- Call To Action -->
        <section class="call-action style-2 bg-img-fix bg-primary">
            <div class="container">
                <div class="inner-content">
                    <div class="row justify-content-between align-items-center">
                        <div class="text-center text-lg-start col-xl-6 m-lg-b20 wow fadeInUp" data-wow-delay="0.2s"
                             style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                            <h2 class="title m-0"><span class="font-weight-400">Subscribe To Our </span>Newsletter</h2>
                        </div>
                        <div class="text-center text-lg-end col-xl-6 wow fadeInUp" data-wow-delay="0.4s"
                             style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                            <form class="dzSubscribe"
                                  action="https://powerzone.dexignzone.com/xhtml/assets/script/mailchamp.php"
                                  method="post">
                                <div class="dzSubscribeMsg"></div>
                                <div class="form-group">
                                    <div class="input-group mb-0">
                                        <div class="input-skew">
                                            <input name="dzEmail" required="required" type="email" class="form-control"
                                                   placeholder="Your Email Address">
                                        </div>
                                        <div class="input-group-addon">
                                            <button name="submit" value="Submit" type="submit"
                                                    class="btn btn-secondary btn-lg btn-skew h-100"><span
                                                        class="skew-inner"><span
                                                            class="text">Subscribe Now</span></span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Call To Action -->

    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>