<?php
$admin_data = $this->md->select('tbl_admin')[0];
$web_data = ($web_data) ? $web_data[0] : '';
$meta_data = $this->md->select_where('tbl_seo', array('page' => $page));

$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';

$unique_id = $this->input->cookie('unique_id');
$cartData = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
?>
<!DOCTYPE html>
<html lang="en">
<?php ob_start(); ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?php echo base_url(); ?>"/>
    <?php
    if (!empty($meta_data)) {
        ?>
        <meta name="author" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="title" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="description" content="<?php echo $meta_data ? $meta_data[0]->description : ''; ?>">
        <meta name="keywords" content="<?php echo $meta_data ? $meta_data[0]->keyword : ''; ?>">
        <title><?php echo $meta_data ? ($meta_data[0]->title . " | " . $web_data->project_name) : (' | ' . $web_data->project_name); ?></title>
        <?php
    } else {
        ?>
        <title><?php echo $page_title . ' | ' . $web_data->project_name; ?></title>
        <?php
        if (isset($blog_data)) {
            ?>
            <meta name="title"
                  content="<?php echo $blog_data[0]->meta_title ? $blog_data[0]->meta_title : $blog_data[0]->title; ?>">
            <meta name="description" content="<?php echo $blog_data[0]->meta_desc; ?>">
            <meta name="keywords" content="<?php echo $blog_data[0]->meta_keyword; ?>">
            <?php
        }
    }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo $page_title . " - " . $web_data->project_name; ?></title>
    <!-- Favicon Icon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link href="<?php echo base_url('admin_asset/'); ?>css/responsive.css" rel="stylesheet">


    <!-- Stylesheet -->
    <link href="assets/vendor/animate/animate.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link href="assets/vendor/magnific-popup/magnific-popup.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="assets/vendor/rangeslider/rangeslider.css" rel="stylesheet">
    <link href="assets/vendor/switcher/switcher.css" rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link class="skin" rel="stylesheet" href="assets/css/skin/skin-2.css">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet">

</head>
<!-- page wrapper -->
<?php $page_head = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- Header -->
<header
    class="site-header mo-left header <?php echo ($active_page == 'index') ? 'style-1' : 'header-transparent style-1'; ?>">
    <!-- Main Header -->
    <div class="sticky-header main-bar-wraper navbar-expand-lg">
        <div class="main-bar clearfix">
            <div class="container-fluid clearfix">
                <div class="clearfix <?php echo ($active_page != 'index') ? 'box-header' : ''; ?>">
                    <!-- Website Logo -->
                    <div class="logo-header mostion logo-dark">
                        <a href="<?php echo base_url() ?>">
                            <img src="<?php echo base_url($web_data->logo ? $web_data->logo : FILENOTFOUND) ?>"
                                 title="<?php echo $web_data->project_name; ?>"
                                 style="width:80px"
                                 alt="<?php echo $web_data->project_name; ?>"/>
                        </a>
                    </div>

                    <!-- Nav Toggle Button -->
                    <button class="navbar-toggler navbar-toggler-skew collapsed navicon justify-content-end"
                            type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
                            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>

                    <!-- Extra Nav -->
                    <div class="extra-nav">
                        <div class="extra-cell">
                            <a href="<?php echo base_url('cart') ?>" class="header-search-btn mr-20"
                               style="position:relative"
                            ><i class="fa-solid fa-cart-shopping font-20"></i>
                                <span class="cartCounter"><?php echo count($cartData); ?></span>
                            </a>
                            <a href="<?php echo base_url('free-consultancy'); ?>"
                               class="btn btn-primary btn-skew appointment-btn"><span
                                    class="skew-inner"><span class="text text-000">Free Consultancy</span></span></a>
                        </div>
                    </div>
                    <!-- Extra Nav -->

                    <!-- Header Nav -->
                    <div class="header-nav navbar-collapse collapse justify-content-center" id="navbarNavDropdown">
                        <div class="logo-header logo-dark">
                            <a href="<?php echo base_url() ?>">
                                <img src="<?php echo base_url($web_data->logo ? $web_data->logo : FILENOTFOUND) ?>"
                                     title="<?php echo $web_data->project_name; ?>"
                                     alt="<?php echo $web_data->project_name; ?>"/>
                            </a>
                        </div>
                        <ul class="nav navbar-nav navbar navbar-left">
                            <li><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li><a href="<?php echo base_url('aboutus'); ?>">About us</a></li>
                            <li><a href="<?php echo base_url('services'); ?>">Services</a></li>
                            <li><a href="<?php echo base_url('package'); ?>">Packages</a></li>
                            <li><a href="<?php echo base_url('gallery'); ?>">Gallery</a></li>
                            <li><a href="<?php echo base_url('contact'); ?>">Contact us</a></li>
                            <li class="d-xs-block d-md-none"><a href="<?php echo base_url('cart'); ?>">Cart</a></li>
                            <li class="d-xs-block d-md-none"><a href="<?php echo base_url('free-consultancy'); ?>">Free
                                    Consultancy</a></li>
                        </ul>
                        <div class="dz-social-icon">
                            <ul>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->facebook; ?>">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->twitter; ?>">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->linkedin; ?>">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->instagram; ?>">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Header End -->
</header>
<!-- Header -->
<?php $page_header = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- Banner  -->
<div class="dz-bnr-inr style-2" style="background-image: url('assets/images/banner/banner-2.png')">
    <div class="banner-gradient"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="dz-bnr-inr-entry">
                    <h1><?php echo ucfirst($page_title); ?></h1>
                    <!-- Breadcrumb Row -->
                    <nav aria-label="breadcrumb" class="breadcrumb-row">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li class="breadcrumb-item active"
                                aria-current="page"><?php echo ucfirst($page_title); ?></li>
                        </ul>
                    </nav>
                    <!-- Breadcrumb Row End -->
                </div>
            </div>
        </div>
        <div class="banner-media">
            <img src="assets/images/banner/pic1.png" alt="/">
        </div>
    </div>
</div>
<!-- Banner End -->
<?php $page_breadcumbs = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- Footer -->
<footer class="site-footer style-1 bg-parallax" style="background-image: url(assets/images/background/footer-bg-2.jpg);"
        id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-12 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="widget widget_about">
                        <div class="footer-logo logo-white">
                            <a href="<?php echo base_url() ?>">
                                <img src="<?php echo base_url($web_data->logo ? $web_data->logo : FILENOTFOUND) ?>"
                                     title="<?php echo $web_data->project_name; ?>"
                                     style="width:100px"
                                     alt="<?php echo $web_data->project_name; ?>"/>
                            </a>
                        </div>
                        <p>The phrase “World Class” cannotes excellence , which is exactly what you will get out of our
                            online coaching. We provide free consultation as per your requirements and questions. </p>
                        <span class="m-b15 d-block text-white font-weight-600">Our Socials</span>
                        <div class="dz-social-icon style-1 dz-hover-move dark">
                            <ul>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->facebook; ?>">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->twitter; ?>">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->linkedin; ?>">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="<?php echo $web_data->instagram; ?>">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="widget recent-posts-entry">
                        <h4 class="footer-title">Contact us</h4>
                        <div class="widget-post-bx">
                            <div class="widget-post clearfix">
                                <div class="dz-info">
                                    <h6 class="title text-white"><a class="text-white"
                                                                    href="##"><?php echo $web_data->address; ?></a></h6>
                                    <span class="post-date"> ADDRESS</span>
                                </div>
                            </div>
                            <div class="post-separator"></div>
                            <div class="widget-post clearfix">
                                <div class="dz-info">
                                    <h6 class="title"><a class="text-white"
                                                         href="tel:<?php echo $web_data->phone; ?>"><?php echo $web_data->phone; ?></a>
                                    </h6>
                                    <span class="post-date"> PHONE</span>
                                </div>
                            </div>
                            <div class="post-separator"></div>
                            <div class="widget-post clearfix">
                                <div class="dz-info">
                                    <h6 class="title"><a class="text-white"
                                                         href="mailto:<?php echo $web_data->email_address; ?>"><?php echo $web_data->email_address; ?></a>
                                    </h6>
                                    <span class="post-date"> EMAIL</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 wow fadeInUp" data-wow-delay="0.8s">
                    <div class="widget widget_working">
                        <h4 class="footer-title">Working Hours</h4>
                        <ul>
                            <li>
                                <span class="days">Monday – Saturday:</span>
                                <span class="time">09.00 – 18.00</span>
                            </li>
                            <li>
                                <span class="days">Sunday:</span>
                                <span class="time">10.00 – 17.00</span>
                            </li>
                        </ul>
                        <a class="btn-link" href="<?php echo base_url('aboutus'); ?>">More Here <i
                                class="fa-solid fa-arrow-right m-l10"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Bottom Part -->
    <div class="container">
        <div class="footer-bottom">
            <div class="text-center">
                <span><?php echo $web_data->footer; ?></span>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->
<div class="scroltop-progress scroltop-primary active-progress">
    <svg width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"
              style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919, 307.919; stroke-dashoffset: 24.3299px;"></path>
    </svg>
</div>
<?php $page_footer = ob_get_clean(); ?>

<?php ob_start(); ?>
<!-- JAVASCRIPT FILES ========================================= -->
<script src="assets/js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
<script src="assets/vendor/wow/wow.js"></script><!-- WOW.JS -->
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- BOOTSTRAP.MIN JS -->
<script src="assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script><!-- BOOTSTRAP SELEECT -->
<script src="assets/vendor/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC POPUP JS -->
<script src="assets/vendor/counter/waypoints-min.js"></script><!-- WAYPOINTS JS -->
<script src="assets/vendor/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
<script src="assets/vendor/tilt/tilt.jquery.min.js"></script><!-- TILT -->
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script><!-- OWL-CAROUSEL -->
<script src="assets/vendor/rangeslider/rangeslider.js"></script><!-- RANGESLIDER -->
<script src="assets/js/dz.carousel.js"></script><!-- OWL-CAROUSEL -->
<script src="assets/js/dz.ajax.js"></script><!-- AJAX -->
<script src="assets/js/custom.js"></script><!-- CUSTOM JS -->
<script src="assets/js/notify.min.js"></script><!-- NOTIFY JS -->
<script>
    /*--------------------
    NOTIFY.JS INITIALIZATION
    ---------------------*/
    $.notify.defaults({
        position: "bottom right",
        gap: '5',
        autoHideDelay: 7000
    });

    /*--------------------
    GET SLOT TIME FROM SLOT ID
   ---------------------*/
    $(document).on("change", "#slotDate", function () {
        let slotDate = $(this).val(); // get slot Date
        var data = {
            slotDate: slotDate
        };
        var url = "<?php echo base_url('Pages/getSlotTime'); ?>";
        jQuery.post(url, data, function (data) {
           $('#slot_time').html(data);
        });
    });

    /*--------------------
    REMOVE TO CART
   ---------------------*/
    $(document).on("click", ".remove-to-cart", function () {
        const ele = $(this);
        let cartId = ele.attr('data-cartid'); // get cart id
        var data = {
            cartId: cartId
        };
        var url = "<?php echo base_url('Pages/removeToCart'); ?>";
        jQuery.post(url, data, function (data) {
            if (parseInt(data) === 2) {
                $.notify("Sorry, something went wrong!", 'error');
            } else if (parseInt(data) === 1) {
                $.notify("Success, Product Removed from cart.", 'success');
            }
            (ele.attr('data-cart') === "true") && (window.location.replace('<?php echo base_url("cart"); ?>'));
        });
    });

</script>
<?php
if (isset($success)) {
    ?>
    <script>
        $.notify('<?php echo $success; ?>', 'success');
    </script>
    <?php
}
if (isset($error)) {
    ?>
    <script>
        $.notify('<?php echo $error; ?>', 'error');
    </script>
    <?php
}

$page_footerscript = ob_get_clean();
$this->load->view($page, array(
    'page_title' => $page_title,
    'page_head' => $page_head,
    'page_header' => $page_header,
    'page_breadcumb' => ($page_breadcumb) ? $page_breadcumbs : '',
    'page_footer' => $page_footer,
    'page_footerscript' => $page_footerscript
));
?>
<style>
    .error-text {
        width: 100%;
        margin-top: 5px;
        font-size: 13px;
        color: #dc3545;
        background: #ff4073e6;
        padding-left: 10px;
    }

    .error-text p {
        margin-bottom: 0 !important;
        color: #ffe7e9 !important;
        margin-left: 12px;
        font-weight: 500;
    }
</style>
</html>