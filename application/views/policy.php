<?php
echo $page_head;
?>
<body>
<div id="wrapper">
<?php echo $page_header; ?>
<?php echo $page_breadcumb; ?>
    <div id="content">
        <!-- Testimonials
   ============================================= -->
        <section class="padding-100">
            <div class="container">
                <div class="row">
                    <!-- Article -->
                    <article>
                        <!-- Main Title -->
                        <?php
                        if (empty($policy)) :
                            echo "Sorry, content not available";
                        else :
                        foreach ($policy as $key => $policy_data) {
                        ?>
                        <div class="col-md-12 mb30">
                           <?php echo $policy_data->policy; ?>
                        </div>
                        <!-- End# Main Title -->
                        <?php }
                        endif;
                        ?>
                    </article>
                    <!-- End# Article -->
                </div>
            </div>
        </section>
        <!-- End Testimonials -->

<?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</div>
</body>