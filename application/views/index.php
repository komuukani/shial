<?php
echo $page_head;
$blogs = $this->md->select_limit_order('tbl_blog', 3, 'blog_id', 'desc');
$gallery = $this->md->select_limit_order('tbl_gallery', 10, 'gallery_id', 'desc');
$reviews = $this->md->select('tbl_review');
$package = $this->md->select('tbl_package');
$web_data = ($web_data) ? $web_data[0] : '';
$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
?>
<body id="bg" class="data-typography-1">
<div id="loading-area" class="loading-page-1">
    <div class="loading-inner">
        <span class="text-white">S</span>
        <span class="text-primary">H</span>
        <span class="text-primary">I</span>
        <span class="text-primary">A</span>
        <span class="text-white">L</span>
    </div>
</div>
<div class="page-wraper">
    <?php echo $page_header; ?>
    <div class="page-content bg-white">

        <?php $this->load->view('slider'); ?>
        <!-- Our Services -->
        <section class="content-inner section-wrapper2">
            <video autoplay loop muted id="video-background">
                <source src="assets/images/background/bg1.mp4" type="video/mp4">
            </video>
            <div class="container">
                <div class="row about-bx2">
                    <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-xl-6 col-md-6 m-b30 wow fadeInUp" data-wow-delay="0.4s">
                                <div class="icon-bx-wraper style-4 box-hover dz-tilt shap-2">
                                    <div class="icon-bx m-b20">
                                        <div class="icon-cell text-primary">
                                            <i class="flaticon-fitness"></i>
                                        </div>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="dz-title m-b10"><a href="##">Our Classes</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.</p>
                                    </div>
                                    <a href="##" class="btn-link">Read More <i
                                            class="fa-solid fa-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 m-b30 wow fadeInUp" data-wow-delay="0.6s">
                                <div class="icon-bx-wraper style-4 box-hover dz-tilt ">
                                    <div class="icon-bx m-b20">
                                        <div class="icon-cell text-primary">
                                            <i class="flaticon-user"></i>
                                        </div>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="dz-title m-b10"><a href="##">Our Trainers</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.</p>
                                    </div>
                                    <a href="##" class="btn-link">Read More <i
                                            class="fa-solid fa-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 m-b30 wow fadeInUp" data-wow-delay="0.8s">
                                <div class="icon-bx-wraper style-4 box-hover dz-tilt ">
                                    <div class="icon-bx m-b20">
                                        <div class="icon-cell text-primary">
                                            <i class="flaticon-medal"></i>
                                        </div>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="dz-title m-b10"><a href="##">Memberships</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.</p>
                                    </div>
                                    <a href="##" class="btn-link">Read More <i
                                            class="fa-solid fa-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 m-b30 wow fadeInUp" data-wow-delay="1.0s">
                                <div class="icon-bx-wraper style-4 box-hover dz-tilt shap-2">
                                    <div class="icon-bx m-b20">
                                        <div class="icon-cell text-primary">
                                            <i class="flaticon-medal"></i>
                                        </div>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="dz-title m-b10"><a href="##">Our Timeline</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                            industry.</p>
                                    </div>
                                    <a href="##" class="btn-link">Read More <i
                                            class="fa-solid fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 m-b30">
                        <div class="dz-media split-box wow fadeInUp" data-wow-delay="1.2s">
                            <img src="assets/images/about/pic9.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="content-inner overflow-hidden">
            <div class="container">
                <div class="row justify-content-between align-items-center m-b40">
                    <div class="text-center text-xl-start col-xl-7 m-lg-b20">
                        <div class="section-head style-3 wow fadeInUp m-0" data-wow-delay="0.4s">
                            <h2 class="title">Our Captivating Gallery</h2>
                        </div>
                    </div>
                    <div class="text-center text-xl-end col-xl-5">
                        <a href="<?php echo base_url('gallery'); ?>" class="btn btn-primary btn-skew m-b30 wow fadeInUp"
                           data-wow-delay="0.6s"><span class="skew-inner"><span class="text">View All</span></span></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-wrapper-inner">
                <div class="swiper portfolio-slider-1 dz-features-wrapper overflow-hidden">
                    <div class="swiper-wrapper dz-features">
                        <?php
                        if (!empty($gallery)) {
                        foreach ($gallery as $gallery_data) {
                        ?>
                        <div class="swiper-slide">
                            <div class="dz-box style-1">
                                <div class="dz-media">
                                    <a href="##"><img src="<?php echo base_url($gallery_data->path ? $gallery_data->path : FILENOTFOUND); ?>"
                                                             alt="<?php echo $gallery_data->title; ?>" width="800" height="650"></a>
                                </div>
                                <div class="dz-content">
                                    <span class="dz-position"><?php echo $gallery_data->title; ?></span>
                                </div>
                            </div>
                        </div>
                        <?php }
                        } ?>
                    </div>
                </div> 
            </div>
        </section>

        <section class="content-inner-2">
            <div class="container">
                <div class="row justify-content-between align-items-center">
                    <div class="text-center text-xl-start col-xl-7 m-lg-b20">
                        <div class="section-head style-3">
                            <h2 class="title wow fadeInUp" data-wow-delay="0.4s">They Are Always Best</h2>
                            <h6 class="sub-title wow fadeInUp m-auto m-xl-0" data-wow-delay="0.6s">Start your training
                                with our Professional Trainers</h6>
                        </div>
                    </div>
                    <div class="text-center text-xl-end col-xl-5">
                        <a href="##" class="btn btn-primary btn-skew m-b30"><span class="skew-inner"><span
                                    class="text">View All</span></span></a>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-6 m-b30">
                        <div class="dz-team style-4 box-hover wow fadeInUp" data-wow-delay="0.8s">
                            <div class="dz-media">
                                <a href="##"><img src="assets/images/team/team-1.png" alt=""></a>
                            </div>
                            <div class="dz-content">
                                <h4 class="dz-name">DESERT</h4>
                                <span class="dz-position">Fitness Trainer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 m-b30">
                        <div class="dz-team style-4 box-hover wow fadeInUp" data-wow-delay="1.0s">
                            <div class="dz-media">
                                <a href="##"><img src="assets/images/team/team-2.png" alt=""></a>
                            </div>
                            <div class="dz-content">
                                <h4 class="dz-name">CHARLES</h4>
                                <span class="dz-position">CROSSFIT COACH</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 m-b30">
                        <div class="dz-team style-4 box-hover wow fadeInUp" data-wow-delay="1.2s">
                            <div class="dz-media">
                                <a href="##"><img src="assets/images/team/team-3.png" alt=""></a>
                            </div>
                            <div class="dz-content">
                                <h4 class="dz-name">JAMES</h4>
                                <span class="dz-position">DUMBBELL TRAINER</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 m-b30">
                        <div class="dz-team style-4 box-hover wow fadeInUp" data-wow-delay="1.4s">
                            <div class="dz-media">
                                <a href="##"><img src="assets/images/team/team-4.png" alt=""></a>
                            </div>
                            <div class="dz-content">
                                <h4 class="dz-name">AMELIA</h4>
                                <span class="dz-position">BOXING TRAINER</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Our Package Start-->
        <section class="content-inner mt-100 bg-999">
            <div class="container">
                <div class="section-head style-2 text-center wow fadeInUp" data-wow-delay="0.4s">
                    <h2 class="title">Choose Your Perfect Package</h2>
                </div>
                <div class="row justify-content-center">
                    <?php
                    if (!empty($package)) {
                        foreach ($package as $package_data) {
                            $url = base_url('package/' . strtolower($package_data->slug));
                            $currency_fromat = (!empty($ipdat->geoplugin_countryName) && ($ipdat->geoplugin_countryName != 'India')) ? '$' : "₹";
                            ?>
                            <div class="col-lg-4 col-md-6 m-b30">
                                <div class="pricingtable-wrapper style-3 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="pricingtable-inner">
                                        <div class="pricingtable-price">
                                            <div class="pricingtable-title"><?php echo $package_data->title; ?></div>
                                            <h2 class="pricingtable-bx"><?php echo $currency_fromat . '' . $package_data->indian_price; ?>
                                                <small>/ <?php echo $package_data->duration; ?></small></h2>

                                            <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>">
                                                <?php
                                                $cartData = $this->md->select_where('tbl_cart', array('package_id' => $package_data->package_id, 'unique_id' => $this->input->cookie('unique_id')));
                                                ?>
                                                <input type="hidden" name="package_id"
                                                       value="<?php echo $package_data->package_id; ?>">
                                                <input type="hidden" name="price"
                                                       value="<?php echo (!empty($ipdat->geoplugin_countryName) && ($ipdat->geoplugin_countryName != 'India')) ? $package_data->overseas_price : $package_data->indian_price; ?>">
                                                <input type="hidden" name="qty"
                                                       value="<?php echo $cartData ? ($cartData[0]->qty + 1) : 1 ?>">

                                                <div class="pricingtable-button">
                                                    <button type="submit" value="send"
                                                            class="btn btn-dark rounded-0 d-block m-b10"><span>Add to cart</span>
                                                    </button>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="pricingtable-features">
                                            <h6 class="pricingtable-sub-title">Features</h6>
                                            <?php echo $package_data->description; ?>
                                        </div>
                                    </div>
                                    <div class="effect"></div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </section>
        <!-- Our Package ENd -->

        <!-- Testimonials-->
        <section class="content-inner testimonial-wrapper1">
            <div class="container">
                <div class="section-head style-2 text-center">
                    <h2 class="title m-b5">Testimonials</h2>

                </div>
            </div>
            <div class="swiper testimonial-swiper-1">
                <div class="swiper-wrapper">
                    <?php
                    if (!empty($reviews)) {
                        foreach ($reviews as $review) {
                            ?>
                            <div class="swiper-slide wow fadeInUp" data-wow-delay="0.2s">
                                <div class="testimonial-1">
                                    <div class="testimonial-info">
                                        <i class="quotes-icon flaticon-quote"></i>
                                        <div class="testimonial-text m-sm-b20 m-b30">
                                            <p><?php echo $review->review; ?></p>
                                        </div>
                                    </div>
                                    <div class="testimonial-details">
                                        <div class="d-flex align-items-center">
                                            <div class="testimonial-pic">
                                                <img src="<?php echo base_url($review->path ? $review->path : FILENOTFOUND); ?>"
                                                     title="<?php echo $review->username; ?>"
                                                     alt="<?php echo $review->username; ?>">
                                            </div>
                                            <div class="info">
                                                <h4 class="testimonial-name"><?php echo $review->username; ?></h4>
                                                <span class="testimonial-position "><?php echo date('d-m, Y', strtotime($review->datetime)); ?></span>
                                            </div>
                                        </div>
                                        <ul class="testimonial-rating">
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                            <li><i class="fa-solid fa-star"></i></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
                <div class="clearfix justify-content-center d-flex m-sm-t30 m-t50">
                    <div class="num-pagination">
                        <div class="swiper-pagination dz-swiper-pagination1 style-2 justify-content-center"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Testimonials-->

    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>