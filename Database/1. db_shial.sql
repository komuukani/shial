-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 06, 2024 at 08:09 AM
-- Server version: 8.0.31
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_shial`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_aboutus`
--

DROP TABLE IF EXISTS `tbl_aboutus`;
CREATE TABLE IF NOT EXISTS `tbl_aboutus` (
  `aboutus_id` int NOT NULL AUTO_INCREMENT,
  `about` text NOT NULL,
  PRIMARY KEY (`aboutus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_aboutus`
--

INSERT INTO `tbl_aboutus` (`aboutus_id`, `about`) VALUES
(8, '<h6><font color=\"#ff0000\" style=\"\">WHO WE ARE</font></h6><p><span style=\"background-color: transparent; color: inherit; font-family: inherit; font-size: 1.5rem; font-weight: 700;\">MEET THE FOUNDER AND THE COACH</span><br></p><p><span style=\"background-color: transparent;\">My father started a gym back in 2008 in India, but at that time fitness industry was not my one true passion. As the years passed and I grew older , I started helping my father with his business . As a result of helping my father , I became more and more dedicated to learning about sports amd fitness with a deeper knowledge . I always believe that a coach is the only person who can lift you up and can take you from 0 to 100 and help you to realise your true potential !</span><br></p><p><span style=\"background-color: transparent;\">More about myself, my name is Divya patel, and i am a certified professional personal training coach, I specialise in nutrition , strength and conditioning, am also very experience and expert in physiotherapy in soft tissue , functional and group training, olymlic weightlifting. I cater all your needs, whatever you like!.</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `admin_id` int NOT NULL AUTO_INCREMENT,
  `user_role_id` int NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL,
  `path` varchar(500) NOT NULL COMMENT 'Profile Photo',
  `address` varchar(200) NOT NULL,
  `bio` varchar(1000) NOT NULL,
  `theme` varchar(10) NOT NULL,
  `lastseen` datetime NOT NULL,
  `register_date` date NOT NULL,
  `status` int NOT NULL COMMENT 'Profile Status',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `user_role_id`, `admin_name`, `email`, `phone`, `password`, `path`, `address`, `bio`, `theme`, `lastseen`, `register_date`, `status`) VALUES
(2, 1, 'admin', 'admin@gmail.com', '(403) 615-2727', '341c3862157a967b269645a552fcf45d65e153abebb3edd4b915837cbca8ca2a1f93c81e382746cd27076e9106654d17232f359fc72c43e428c2649dbc9c8990crWE3iM4+YPFHWxae4L9Q9pnXdWofBy6AuSkgPL7flY=', './admin_asset/profile/43a210531452ba5c8faa71f71ff88290.png', '5075 Falconridge Blvd NE # 521, Calgary, AB, Canada, Alberta', '<p>Hello I am admin</p>', 'theme-3', '2023-10-03 03:05:19', '2022-09-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

DROP TABLE IF EXISTS `tbl_banner`;
CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `banner_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `subtitle` varchar(500) NOT NULL,
  `path` varchar(1000) NOT NULL,
  `position` int NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`banner_id`, `title`, `subtitle`, `path`, `position`) VALUES
(40, 'Beauty Inspired <br> by Real Life', 'Made using clean, non-toxic ingredients, our products are designed for everyone.', './admin_asset/banner/debf11b93c7cf3290d48768c639b53a5.png', 1),
(41, 'Get The Perfectly <br> Hydrated Skin', 'Made using clean, non-toxic ingredients, our products are designed for everyone.', './admin_asset/banner/10d1babe346753e95269182f72f3acb4.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

DROP TABLE IF EXISTS `tbl_blog`;
CREATE TABLE IF NOT EXISTS `tbl_blog` (
  `blog_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `path` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_keyword` varchar(500) NOT NULL,
  `meta_desc` varchar(500) NOT NULL,
  `blogdate` date NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`blog_id`, `title`, `slug`, `path`, `description`, `meta_title`, `meta_keyword`, `meta_desc`, `blogdate`) VALUES
(11, 'Vitamin E', 'vitamin-e', './admin_asset/blog/df7f0cc7a67bb82dc9a53367e8e43e8f.jpeg', '<p>Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.</p><p>Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.<span style=\"background-color: transparent;\">with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.</span></p><p>Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.<br></p>', 'Vitamin E', 'Just about everyone knows about Vitamin E.  I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.  It is not.', 'Just about everyone knows about Vitamin E.  I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.  It is not.', '2023-09-29'),
(12, 'How do I use skincare products', 'how-do-i-use-skincare-products', './admin_asset/blog/b8ff6bb6f328fbb13fdd2dbac6006b8a.jpg', 'Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.', 'How do I use skincare products', 'If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.', 'If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.', '2023-09-29'),
(13, 'THE SECRET OF SERUMS', 'the-secret-of-serums', './admin_asset/blog/6ceff2ad3b715a8365892696a2be672a.jpg', '<p>Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.</p><p>Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.<br></p>', 'THE SECRET OF SERUMS', 'Serums offer a way to deliver high quality actives to the deeper layers of the skin.  One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.', 'Serums offer a way to deliver high quality actives to the deeper layers of the skin.  One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.', '2023-09-29'),
(14, 'WHAT’S REALLY IN MY SKIN CARE PRODUCTS?', 'whats-really-in-my-skin-care-products', './admin_asset/blog/63298da039f25745183038836609615c.jpg', '<p>Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.</p><p>Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.</p><p>Managing multiple student loans can be overwhelming, with varying interest rates, payment due dates, and loan servicers to keep track of. Consolidating your student loans offers several benefits that can simplify repayment and potentially save you money. In this article, we will explore the advantages of consolidating your student loans and how it can positively impact your financial situation.<br></p>', 'WHAT’S REALLY IN MY SKIN CARE PRODUCTS?', 'What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.', 'What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.', '2023-09-29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

DROP TABLE IF EXISTS `tbl_cart`;
CREATE TABLE IF NOT EXISTS `tbl_cart` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `package_id` int DEFAULT NULL,
  `unique_id` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int DEFAULT NULL,
  `netprice` double DEFAULT NULL,
  `entry_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`cart_id`, `package_id`, `unique_id`, `price`, `qty`, `netprice`, `entry_date`, `modify_date`) VALUES
(11, 3, '7v43cvr08a8l6t89kjuo9l7pd6d0st4j', 10000, 1, 10000, '2024-04-05 21:25:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `position` int NOT NULL,
  `description` longtext NOT NULL,
  `path` varchar(500) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `title`, `position`, `description`, `path`) VALUES
(6, 'Chicken  Curry', 3, '', './admin_asset/category/1885eb24bd4a55eb88036b769e6dbb72.jpeg'),
(7, 'Vegetarian  Curry', 2, '', './admin_asset/category/410692732284b9243b99849ea9a232b8.jpg'),
(8, 'Biryani', 4, '', './admin_asset/category/784b9f6f8f4f1847c0a36e2ce305aac9.jpg'),
(9, 'Vegetarian ki entrees', 1, '', './admin_asset/category/ee0a8f6b6d2ae129beba7ba2083832be.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

DROP TABLE IF EXISTS `tbl_client`;
CREATE TABLE IF NOT EXISTS `tbl_client` (
  `client_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `path` varchar(500) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_client`
--

INSERT INTO `tbl_client` (`client_id`, `title`, `path`) VALUES
(10, 'Demo Client', './admin_asset/client/46fa3ab5f0eb5af7f7af9bafbf83c0fb.png'),
(11, 'parttner 2', './admin_asset/client/e90f4d7acfbc7d3dcc22d3b11d035cca.png'),
(12, 'insta', './admin_asset/client/445a04d7ee0a92883a17317216b2e482.png'),
(13, 'raja', './admin_asset/client/d9eb75fc13f75c1955d06215d8dda11a.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `contact_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `subject` varchar(2000) NOT NULL,
  `message` varchar(2000) NOT NULL,
  `datetime` varchar(50) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `name`, `email`, `phone`, `subject`, `message`, `datetime`) VALUES
(27, 'nishant', 'nishant@gmail.com', '789461320', '', 'demo mail', '2023-08-24 17:50:23'),
(28, 'nishant thummar', 'nishant@gmail.com', '5783453489', '', 'demo mail', '2023-08-24 17:51:25'),
(31, 'anuj', 'anuj@gmail.com', '789456130', 'Question or Comment', 'demo mail', '2023-08-31 19:49:02'),
(32, 'nishatn', 'nishatn@gmail.com', '8460124263', '', 'sdfsdfsdf', '2023-10-01 19:29:09'),
(33, 'nisahtn', 'nishant@gmail.com', '465789231', '', 'ti si demo', '2023-10-20 13:49:36'),
(34, 'nisahtn', 'nishant@gmail.com', '465789231', '', 'ti si demo', '2023-10-20 13:50:12'),
(35, 'nisahtn', 'nishant@gmail.com', '465789231', '', 'ti si demo', '2023-10-20 13:53:03'),
(36, 'nisahtn', 'nishant@gmail.com', '465789231', '', 'ti si demo', '2023-10-20 13:55:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_control`
--

DROP TABLE IF EXISTS `tbl_control`;
CREATE TABLE IF NOT EXISTS `tbl_control` (
  `control_id` int NOT NULL AUTO_INCREMENT,
  `aboutus` enum('1','0') NOT NULL,
  `banner` enum('1','0') NOT NULL,
  `blog` enum('1','0') NOT NULL,
  `category` enum('1','0') NOT NULL,
  `client` enum('1','0') NOT NULL,
  `email_subscriber` enum('1','0') NOT NULL,
  `contact` enum('1','0') NOT NULL,
  `feedback` enum('1','0') NOT NULL,
  `gallery` enum('1','0') NOT NULL,
  `inquiry` enum('1','0') NOT NULL,
  `mainmenu` enum('1','0') NOT NULL,
  `permission` enum('1','0') NOT NULL,
  `policy` enum('1','0') NOT NULL,
  `review` enum('1','0') NOT NULL,
  `role` enum('1','0') NOT NULL,
  `seo` enum('1','0') NOT NULL,
  `subcategory` enum('1','0') NOT NULL,
  `submenu` enum('1','0') NOT NULL,
  `terms` enum('1','0') NOT NULL,
  `team` enum('1','0') NOT NULL,
  `user` enum('1','0') NOT NULL,
  `video` enum('1','0') NOT NULL,
  PRIMARY KEY (`control_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_control`
--

INSERT INTO `tbl_control` (`control_id`, `aboutus`, `banner`, `blog`, `category`, `client`, `email_subscriber`, `contact`, `feedback`, `gallery`, `inquiry`, `mainmenu`, `permission`, `policy`, `review`, `role`, `seo`, `subcategory`, `submenu`, `terms`, `team`, `user`, `video`) VALUES
(1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_control_feature`
--

DROP TABLE IF EXISTS `tbl_control_feature`;
CREATE TABLE IF NOT EXISTS `tbl_control_feature` (
  `control_feature_id` int NOT NULL AUTO_INCREMENT,
  `general` varchar(200) NOT NULL,
  `social` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `security` varchar(200) NOT NULL,
  `system` varchar(200) NOT NULL,
  `captcha` varchar(200) NOT NULL,
  `theme` varchar(200) NOT NULL,
  `payment` varchar(200) NOT NULL,
  `language` varchar(200) NOT NULL,
  `backup` varchar(200) NOT NULL,
  PRIMARY KEY (`control_feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_control_feature`
--

INSERT INTO `tbl_control_feature` (`control_feature_id`, `general`, `social`, `email`, `security`, `system`, `captcha`, `theme`, `payment`, `language`, `backup`) VALUES
(1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(2, 'fas fa-cog', 'fas fa-search', 'fas fa-envelope', 'fas fa-lock', 'fas fa-power-off', 'fas fa-sync-alt', 'fas fa-palette', 'far fa-credit-card', 'fas fa-language', 'fas fa-database'),
(3, 'General settings such as, site title, site description, address and so on.', 'Search engine optimization settings, such as meta tags and social media.', 'Email SMTP settings, notifications and others related to email.', 'Security settings such as change admin login password and others.', 'PHP version settings, time zones and other environments.', 'Google Captcha Security settings for contact, inquiry and other form.', 'Change admin theme and other appearance changes as per your choice.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Set the backup settings below');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_subscriber`
--

DROP TABLE IF EXISTS `tbl_email_subscriber`;
CREATE TABLE IF NOT EXISTS `tbl_email_subscriber` (
  `email_subscriber_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(500) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_subscriber_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email_subscriber`
--

INSERT INTO `tbl_email_subscriber` (`email_subscriber_id`, `email`, `datetime`) VALUES
(9, 'example1@mail.com', '2022-10-06 12:50:35'),
(10, 'example2@mail.com', '2022-10-06 12:50:35'),
(11, 'fgfg@sdf.dfgfd', '2023-09-30 18:55:03'),
(12, 'sdf@dsf.sdf', '2023-10-19 15:17:06'),
(13, 'dfgfdg@dsf.dsfs', '2023-10-19 15:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

DROP TABLE IF EXISTS `tbl_gallery`;
CREATE TABLE IF NOT EXISTS `tbl_gallery` (
  `gallery_id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `path` varchar(2000) NOT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`gallery_id`, `category`, `title`, `path`) VALUES
(19, '', 'Test 1', './admin_asset/gallery/4edeb5fb21edca4b72d6475d5c4eee94.jpg'),
(20, '', '', './admin_asset/gallery/487c7b85ce685c3502d1fccec64ee2dd.jpg'),
(21, '', '', './admin_asset/gallery/2cac170e242a315653c89ded591287de.jpg'),
(22, '', '', './admin_asset/gallery/b1a9f6a0744c6f977fa899ca7e26ae1e.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mainmenu`
--

DROP TABLE IF EXISTS `tbl_mainmenu`;
CREATE TABLE IF NOT EXISTS `tbl_mainmenu` (
  `mainmenu_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `position` int NOT NULL,
  `icon` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`mainmenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mainmenu`
--

INSERT INTO `tbl_mainmenu` (`mainmenu_id`, `title`, `slug`, `position`, `icon`, `controller`, `url`, `status`) VALUES
(22, 'Manage FAQ\'s', 'manage-faq/?(:any)?/?(:any)?', 1, 'fas fa-question-circle', 'faq', 'Admin/Pages/faq/$2/$3', 1),
(24, 'Manage Products', 'manage-product/?(:any)?/?(:any)?', 3, 'fab fa-product-hunt', 'product', 'Admin/Pages/product/$2/$3', 1),
(27, 'Manage Reservation', 'manage-reservation/?(:any)?/?(:any)?', 3, 'fas fa-wine-glass-alt', 'reservation', 'Admin/Pages/reservation/$2/$3	', 1),
(29, 'Packages', 'manage-package/?(:any)?/?(:any)?', 4, 'fas fa-dollar-sign', 'package', 'Admin/Pages/package/$2/$3', 1),
(30, 'Slots', 'manage-slot/?(:any)?/?(:any)?', 5, 'far fa-calendar-check', 'slot', 'Admin/Pages/slot/$2/$3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `menu_id` int NOT NULL AUTO_INCREMENT,
  `text` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `href` varchar(200) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'slug',
  `target` varchar(10) NOT NULL,
  `parent` int NOT NULL,
  `position` int NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

DROP TABLE IF EXISTS `tbl_package`;
CREATE TABLE IF NOT EXISTS `tbl_package` (
  `package_id` int NOT NULL AUTO_INCREMENT,
  `title` char(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` char(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `indian_price` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `overseas_price` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(4000) NOT NULL,
  `path` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_package`
--

INSERT INTO `tbl_package` (`package_id`, `title`, `slug`, `indian_price`, `overseas_price`, `duration`, `description`, `path`, `status`, `entry_date`, `modify_date`) VALUES
(1, 'PERSONAL TRAINING FACE TO FACE', 'personal-training-face-to-face', '8000', '10000', '4 WEEKS', '<ul class=\"elementor-price-table__features-list\"><li>1 Month Price</li><li class=\"elementor-repeater-item-13d9a1a\"><div class=\"elementor-price-table__feature-inner\">High class video call training</div></li><li class=\"elementor-repeater-item-38b59c2\"><div class=\"elementor-price-table__feature-inner\">Personalized meal plans</div></li><li class=\"elementor-repeater-item-f9494a2\"><div class=\"elementor-price-table__feature-inner\">Daily unique design workout plans</div></li><li class=\"elementor-repeater-item-eae42de\"><div class=\"elementor-price-table__feature-inner\">Time as per your convinence</div></li><li class=\"elementor-repeater-item-ff9495d\"><div class=\"elementor-price-table__feature-inner\">24×7 coach support</div></li><li class=\"elementor-repeater-item-b158ab9\"><div class=\"elementor-price-table__feature-inner\">For fat loss , muscle gain, or maintaining weight & flexible body</div></li><li class=\"elementor-repeater-item-a429d3e\"><div class=\"elementor-price-table__feature-inner\">6 sessions a week ( 50 mins per session) through video calls</div></li><li class=\"elementor-repeater-item-ba400fe\"><div class=\"elementor-price-table__feature-inner\">Supplements guidance</div></li></ul>', './admin_asset/package/9915328e8f599e080d6659ee6c6bfa7c.jpg', 1, '2024-04-04', '2024-04-04'),
(2, 'NUTRITION COUNSELING', 'nutrition-counseling', '2000', '4000', '4 WEEKS', '<ul class=\"elementor-price-table__features-list\"><li class=\"elementor-repeater-item-13d9a1a\"><div class=\"elementor-price-table__feature-inner\">1 Month Price</div></li><li class=\"elementor-repeater-item-13d9a1a\"><div class=\"elementor-price-table__feature-inner\">Personalized meal plan</div></li><li class=\"elementor-repeater-item-38b59c2\"><div class=\"elementor-price-table__feature-inner\">Includes for vegetarian, non vegetarian, eggetarian and vegan</div></li><li class=\"elementor-repeater-item-f9494a2\"><div class=\"elementor-price-table__feature-inner\">Easy, tasty recipes and foods</div></li><li class=\"elementor-repeater-item-eae42de\"><div class=\"elementor-price-table__feature-inner\">Budget friendly</div></li><li class=\"elementor-repeater-item-ff9495d\"><div class=\"elementor-price-table__feature-inner\">For fat loss , musle gains , weight loss or weight gain</div></li><li class=\"elementor-repeater-item-b158ab9\"><div class=\"elementor-price-table__feature-inner\">Weekly checkins</div></li><li class=\"elementor-repeater-item-a429d3e\"><div class=\"elementor-price-table__feature-inner\">Supplements guidance</div></li></ul>', './admin_asset/package/1fb5da6cc052f4093e163c9122d3808d.jpg', 1, '2024-04-04', '0000-00-00'),
(3, 'STRENGTH AND CONDITIONING TRAINING', 'strength-and-conditioning-training', '10000', '12000', '4 WEEKS', '<ul class=\"elementor-price-table__features-list\"><li>1 Month Price</li><li class=\"elementor-repeater-item-13d9a1a\"><div class=\"elementor-price-table__feature-inner\">Strength training specific to your sports</div></li><li class=\"elementor-repeater-item-38b59c2\"><div class=\"elementor-price-table__feature-inner\">The coach will personally research to that particular sports</div></li><li class=\"elementor-repeater-item-f9494a2\"><div class=\"elementor-price-table__feature-inner\">Daily different designs workout plans</div></li><li class=\"elementor-repeater-item-eae42de\"><div class=\"elementor-price-table__feature-inner\">4 days a week ( 1hour per session) 24×7 coach support</div></li></ul>', './admin_asset/package/3e3e3d0d1172be16c3b54448223eef5a.jpg', 1, '2024-04-04', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission`
--

DROP TABLE IF EXISTS `tbl_permission`;
CREATE TABLE IF NOT EXISTS `tbl_permission` (
  `permission_id` int NOT NULL AUTO_INCREMENT,
  `menu` char(50) NOT NULL,
  `type` varchar(15) NOT NULL,
  `feature` varchar(30) NOT NULL,
  `all` enum('0','1') NOT NULL,
  `read` enum('0','1') NOT NULL,
  `write` enum('0','1') NOT NULL,
  `edit` enum('0','1') NOT NULL,
  `delete` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permission`
--

INSERT INTO `tbl_permission` (`permission_id`, `menu`, `type`, `feature`, `all`, `read`, `write`, `edit`, `delete`, `status`) VALUES
(1, 'site pages', 'page', 'contact', '0', '1', '0', '0', '1', '0'),
(2, 'site pages', 'page', 'aboutus', '1', '1', '1', '1', '1', '0'),
(3, 'site pages', 'page', 'banner', '1', '1', '1', '1', '1', '0'),
(4, 'site pages', 'page', 'gallery', '1', '1', '1', '1', '1', '0'),
(5, 'site pages', 'page', 'client', '1', '1', '1', '1', '1', '0'),
(6, 'site pages', 'page', 'terms', '1', '1', '1', '1', '1', '0'),
(7, 'site pages', 'page', 'policy', '1', '1', '1', '1', '1', '0'),
(8, 'site pages', 'page', 'review', '1', '1', '1', '1', '1', '0'),
(9, 'site pages', 'page', 'blog', '1', '1', '1', '1', '1', '0'),
(10, 'site pages', 'page', 'feedback', '0', '1', '0', '0', '1', '0'),
(11, 'site pages', 'page', 'team', '1', '1', '1', '1', '1', '0'),
(12, 'site pages', 'page', 'video', '1', '1', '1', '1', '1', '0'),
(13, 'site pages', 'page', 'inquiry', '0', '1', '0', '0', '1', '0'),
(14, 'site pages', 'page', 'seo', '1', '1', '1', '1', '1', '0'),
(15, 'site pages', 'page', 'category', '1', '1', '1', '1', '1', '0'),
(16, 'site pages', 'page', 'subcategory', '1', '1', '1', '1', '1', '0'),
(17, 'site pages', 'page', 'user', '1', '1', '1', '1', '1', '1'),
(18, 'site pages', 'page', 'role', '1', '1', '1', '1', '1', '0'),
(19, 'site pages', 'page', 'permission', '1', '1', '1', '1', '1', '0'),
(20, 'site pages', 'page', 'mainmenu', '1', '1', '1', '1', '1', '0'),
(21, 'site pages', 'page', 'submenu', '1', '1', '1', '1', '1', '0'),
(22, '', 'feature', 'general', '1', '1', '0', '1', '0', '0'),
(23, '', 'feature', 'social', '1', '1', '0', '1', '0', '0'),
(24, '', 'feature', 'email', '1', '1', '0', '1', '0', '0'),
(25, '', 'feature', 'security', '1', '1', '0', '1', '0', '0'),
(26, '', 'feature', 'system', '1', '1', '0', '1', '0', '0'),
(27, '', 'feature', 'captcha', '1', '1', '0', '1', '0', '0'),
(28, '', 'feature', 'theme', '1', '1', '0', '1', '0', '0'),
(29, '', 'feature', 'payment', '1', '1', '0', '1', '0', '0'),
(30, '', 'feature', 'language', '1', '1', '0', '1', '0', '0'),
(31, '', 'feature', 'backup', '1', '1', '0', '1', '0', '0'),
(39, 'faq', 'page', 'faq', '1', '1', '1', '1', '1', '1'),
(41, 'product', 'page', 'product', '1', '1', '1', '1', '1', '1'),
(44, 'reservation', 'page', 'reservation', '1', '1', '1', '1', '1', '1'),
(45, 'email', 'page', 'email', '1', '1', '1', '1', '1', '1'),
(46, 'package', 'page', 'package', '1', '1', '1', '1', '1', '1'),
(47, 'slot', 'page', 'slot', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission_assign`
--

DROP TABLE IF EXISTS `tbl_permission_assign`;
CREATE TABLE IF NOT EXISTS `tbl_permission_assign` (
  `permission_assigned_id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL,
  `permission_id` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `all` enum('1','0') NOT NULL,
  `read` enum('1','0') NOT NULL,
  `write` enum('1','0') NOT NULL,
  `edit` enum('1','0') NOT NULL,
  `delete` enum('1','0') NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`permission_assigned_id`),
  KEY `Property ID` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=618 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permission_assign`
--

INSERT INTO `tbl_permission_assign` (`permission_assigned_id`, `role_id`, `permission_id`, `type`, `all`, `read`, `write`, `edit`, `delete`, `status`) VALUES
(609, 1, '[28,27,26,25,23,22]', 'feature', '1', '1', '1', '1', '1', '1'),
(610, 1, '[28,27,26,25,23,22]', 'feature', '0', '1', '0', '0', '0', '0'),
(611, 1, '[28,27,26,25,23,22]', 'feature', '0', '0', '0', '1', '0', '0'),
(612, 1, '[47,46,8,4,3,2]', 'page', '1', '1', '1', '1', '1', '1'),
(613, 1, '[46,8,4,3,2,1]', 'page', '0', '1', '0', '0', '0', '0'),
(614, 1, '[46,8,4,3,2]', 'page', '0', '0', '1', '0', '0', '0'),
(615, 1, '[46,8,4,3,2]', 'page', '0', '0', '0', '1', '0', '0'),
(616, 1, '[46,8,4,3,2,1]', 'page', '0', '0', '0', '0', '1', '0'),
(617, 1, '[46]', 'page', '0', '0', '0', '0', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_policy`
--

DROP TABLE IF EXISTS `tbl_policy`;
CREATE TABLE IF NOT EXISTS `tbl_policy` (
  `policy_id` int NOT NULL AUTO_INCREMENT,
  `policy` text NOT NULL,
  PRIMARY KEY (`policy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_policy`
--

INSERT INTO `tbl_policy` (`policy_id`, `policy`) VALUES
(4, '<div class=\"elementor-element elementor-element-53725c0 elementor-widget elementor-widget-heading\" data-id=\"53725c0\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h4 style=\"font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">PRIVACY POLICY AND NOTICE</h4></div></div><div class=\"elementor-element elementor-element-29256c5 elementor-widget elementor-widget-text-editor\" data-id=\"29256c5\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">This is the privacy notice of Skin-2-Love. In this document, “we”, “our”, or “us” refers to&nbsp;Clowder Creations Ltd.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We are registered in New Zealand and our company number is 8267621.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Our registered office is at Unit 1 – 3 / 4 Carrowmore, Pinehill, 0632, Auckland, New Zealand</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">This privacy notice aims to inform you about how we collect, store, and handle any information that we collect from you, or that you provide to us. It covers both information that could identify you (“personal information”) and information that could not.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">A core focus for us is trading in a conscientious way and keeping your personal detail private and confidential.&nbsp;&nbsp; Your details will never be shared, sold, or disclosed with a third party/parties – not ever!</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Let us know if you need any further clarification.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Here is a list of the information we collect from you, either through our website or because you give it to us in some other way, and why it is necessary to collect it:</p></div></div><div class=\"elementor-element elementor-element-5588709 elementor-widget elementor-widget-heading\" data-id=\"5588709\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h4 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">BUSINESS AND PERSONAL INFORMATION</h4></div></div><div class=\"elementor-element elementor-element-9c2b07c elementor-widget elementor-widget-text-editor\" data-id=\"9c2b07c\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">This includes basic identification and contact information, such as your name and contact details, and also includes all information given to us in the course of your business and ours, such as information you give us in your capacity as our client. We undertake to preserve the confidentiality of the information and of the terms of our relationship. It is not used for any other purpose. We expect you to reciprocate this policy.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">This information is used:</p><ul style=\"margin: 15px 0px 15px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">to provide you with the goods which you order,</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">for verifying your identity for security purposes,</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">for marketing our products,</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">the information which does not identify any individual may be used in a general way by us to provide class information, for example relating to demographics or usage of a particular page or service.</li></ul><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We keep the information, which forms part of our business record for a minimum of seven years. This is the period specified by the tax collecting authorities and other legal entities in the event of a claim or defense in court.</p></div></div><div class=\"elementor-element elementor-element-03c60cb elementor-widget elementor-widget-heading\" data-id=\"03c60cb\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h4 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">YOUR DOMAIN NAME AND E-MAIL ADDRESS</h4></div></div><div class=\"elementor-element elementor-element-9741505 elementor-widget elementor-widget-text-editor\" data-id=\"9741505\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">This information is recognized by our servers and the pages that you visit are recorded. We shall not under any circumstances, divulge your e-mail address to any person who is not an employee or contractor of ours and who does not need to know, either generally or specifically. This information is used:</p><ul style=\"margin: 15px 0px 15px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">to correspond with you or deal with you regarding the goods offered on our site.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">in a collective way not referable to any individual, for the purpose of quality control and improvement of our website.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">to send you news about the services for which you have signed up.</li></ul></div></div><div class=\"elementor-element elementor-element-74ce903 elementor-widget elementor-widget-heading\" data-id=\"74ce903\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h4 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">INFORMATION YOU POST ON OUR WEBSIT</h4></div></div><div class=\"elementor-element elementor-element-841fb6c elementor-widget elementor-widget-text-editor\" data-id=\"841fb6c\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Information you send to us by posting to a forum or blog is stored on our servers. We do not specifically use that information except to allow it to be read, however, we reserve the right to use or refer to that information in whatever way we see fit.</p></div></div><div class=\"elementor-element elementor-element-ce468fd elementor-widget elementor-widget-heading\" data-id=\"ce468fd\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h4 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">WEBSITE USAGE INFORMATION</h4></div></div><div class=\"elementor-element elementor-element-56d05c2 elementor-widget elementor-widget-text-editor\" data-id=\"56d05c2\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We may use software embedded in our website (such as JavaScript) to collect information about pages you view and how you have reached them, what you do when you visit a page, the length of time you remain on the page, and how we perform in providing content to you. No personally identifiable information is used or retained by this software.</p></div></div><div class=\"elementor-element elementor-element-2bc0285 elementor-widget elementor-widget-heading\" data-id=\"2bc0285\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h4 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">FINANCIAL INFORMATION RELATING TO YOUR CREDIT CARDS</h4></div></div><div class=\"elementor-element elementor-element-3858e77 elementor-widget elementor-widget-text-editor\" data-id=\"3858e77\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">This information is never taken by us either through our website or otherwise. At the point of payment, you are transferred to a secure page on the website of Windcave or some other reputable payment service provider. This page may be made up to look like our website, but it is completely secured and not controlled by us.&nbsp; Neither our staff nor contractors have access to the site.</p></div></div><div class=\"elementor-element elementor-element-f5b51b1 elementor-widget elementor-widget-heading\" data-id=\"f5b51b1\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h4 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">FINANCIAL INFORMATION RELATING TO YOUR CREDIT CARDS</h4></div></div><div class=\"elementor-element elementor-element-c44fe0b elementor-widget elementor-widget-text-editor\" data-id=\"c44fe0b\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We use Secure Sockets Layer (SSL) certificates to verify your identity in your browser and to encrypt any data you give us when you buy. This includes financial information such as credit or debit card numbers. Our SSL certificate encryption level is 128-bit/256-bit. Whenever we ask for financial information, you can check that SSL is being used by looking for a closed padlock symbol or another trust mark in your browser URL bar or toolbar.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We take the following measures to protect your financial information:</p><ul style=\"margin: 15px 0px 15px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We keep your financial information encrypted on our servers.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Only partial information from orders is kept ensuring order integrity.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Access to your information is restricted to authorized staff only.</li><li style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">If we ask you questions about your financial information, we shall show partial detail [the first four OR the last four digits of the debit or credit card number], only enough to identify the card(s) to which we refer.</li></ul></div></div><div class=\"elementor-element elementor-element-f7c3ffe elementor-widget elementor-widget-heading\" data-id=\"f7c3ffe\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">CREDIT REFERENCE</h2></div></div><div class=\"elementor-element elementor-element-c6da193 elementor-widget elementor-widget-text-editor\" data-id=\"c6da193\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">To assist in combating fraud, we share information with credit reference agencies, so far as it relates to clients or customers who instruct their credit card issuer to cancel payment to us without having first provided an acceptable reason to us and given us the opportunity to refund their money.</p></div></div><div class=\"elementor-element elementor-element-2c05e5a elementor-widget elementor-widget-heading\" data-id=\"2c05e5a\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">THIRD PARTY CONTENT</h2></div></div><div class=\"elementor-element elementor-element-b65e50d elementor-widget elementor-widget-text-editor\" data-id=\"b65e50d\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Our website is a publishing medium in that anyone may register and then publish information about himself or some other person. We do not moderate or control what is posted.&nbsp; All complaints will be investigated, and post-removed until such time as the issue had been investigated.&nbsp; We will not interfere with free speech and opinions are tolerated as long as they are not presented in a malicious or overhanded way.&nbsp; We will not tolerate any hate speech or personal attacks against any person, regarding race, religion, sexual orientation, or any other discrimination. If we find your complaint to be without cause, we reserve the right to reinstate the post without corresponding with you.</p></div></div><div class=\"elementor-element elementor-element-8892e19 elementor-widget elementor-widget-heading\" data-id=\"8892e19\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">INFORMATION WE OBTAIN FROM THIRD PARTIES</h2></div></div><div class=\"elementor-element elementor-element-52ae6ef elementor-widget elementor-widget-text-editor\" data-id=\"52ae6ef\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Although we do not disclose your personal information to any third party (except as set out in this notice), we do receive data that is indirectly made up of your personal information, from software services such as Google Analytics and others. No such information is identifiable to you.</p></div></div><div class=\"elementor-element elementor-element-3ccf544 elementor-widget elementor-widget-heading\" data-id=\"3ccf544\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 cl');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `title` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `path` varchar(500) COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL,
  `slug` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `ingredients` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `product_type` char(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `price` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `category_id`, `title`, `path`, `description`, `status`, `entry_date`, `modify_date`, `slug`, `ingredients`, `product_type`, `price`) VALUES
(19, 9, 'Vegetable Samosa ', './admin_asset/product/5b03efa910ffc81c17a4b58acbcfd9b6.jpeg', 'A deep-fried conical pastry stuffed with spiced potatoes and peas', 1, '2023-10-14', '0000-00-00', 'vegetable-samosa', 'A deep-fried conical pastry stuffed with spiced potatoes and peas', 'Dine In', '50'),
(20, 9, 'Potato Pakora', './admin_asset/product/1412cd2f38fe8ef8d9364eb04d1d4b4c.jpeg', 'Potato mixes with gram flour batter and deep fried', 1, '2023-10-14', '0000-00-00', 'potato-pakora', 'Potato mixes with gram flour batter and deep fried', 'Take Away', '200'),
(21, 9, 'Onion Bhaji', './admin_asset/product/d4820b1f252afc1ac92b5c43720fb6a6.jpeg', 'spiced Onion mix with chickpeas and rice flour batter and deep fried', 1, '2023-10-14', '0000-00-00', 'onion-bhaji', 'spiced Onion mix with chickpeas and rice flour batter and deep fried', 'Both', '100'),
(22, 8, 'Kashmiri Pulav', './admin_asset/product/d94674d67077a24e9f1cc4b78477d388.jpg', 'A delicious variant of rice pulao from Kashmiri cuisine made with nuts, \r\ndried fruits, and saffron', 1, '2023-10-14', '0000-00-00', 'kashmiri-pulav', 'A delicious variant of rice pulao from Kashmiri cuisine made with nuts, \r\ndried fruits, and saffron', 'Dine In', '200'),
(23, 8, 'Chicken Biryani', './admin_asset/product/10f386f7bdaa0916a258dcc5b2f3fa4d.jpg', 'Chicken cooked with spices in rice, flavored with saffron', 1, '2023-10-14', '0000-00-00', 'chicken-biryani', 'Chicken cooked with spices in rice, flavored with saffron', 'Dine In', '600'),
(24, 7, 'Dal Tadka', './admin_asset/product/6b3de1e428644be52e1f26d0f2507523.jpg', 'Yellow lentils cooked with onion, coriander, cumin, tomato, and spices', 1, '2023-10-14', '0000-00-00', 'dal-tadka', 'Yellow lentils cooked with onion, coriander, cumin, tomato, and spices', 'Dine In', '800'),
(25, 7, 'Dal Makhni', './admin_asset/product/92f992160464493bfcba7efcd4ed269d.jpg', 'Black lentils cooked overnight with onion, ginger, garlic, Indian spices, and butter', 1, '2023-10-14', '2023-10-14', 'dal-makhni', 'Black lentils cooked overnight with onion, ginger, garlic, Indian spices, and butter', 'Take Away', '800'),
(26, 7, 'Fresh Garden Vegetable Curry', './admin_asset/product/d2526a4f9a56853dda733d1a1e6b4fd5.jpg', 'Fresh seasonal vegetable cooked with ginger, onion, tomato, capsicum, and finished with \r\nfreshly ground spices', 1, '2023-10-14', '2023-10-14', 'fresh-garden-vegetable-curry', 'Fresh seasonal vegetable cooked with ginger, onion, tomato, capsicum, and finished with \r\nfreshly ground spices', 'Both', '500'),
(27, 9, 'demo', './admin_asset/product/d351bcc8b20ea3b18201ffca020fb3fa.jpg', 'ghfgh', 1, '2023-10-17', '0000-00-00', 'demo', 'tertert', 'Take Away', '45'),
(28, 9, 'दीवाने खास सम्ब्जी', '', '', 1, '2023-10-19', '2023-10-19', '', 'दीवाने खास सम्ब्जी  e flour batter and deep friedspiced Onion mix with chickpeas and rice flour batter and deep friedspiced Onion mix with chickpeas and rice flour batter and deep friedspiced Onion mix with chickp.', 'Dine In', '4564');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservation`
--

DROP TABLE IF EXISTS `tbl_reservation`;
CREATE TABLE IF NOT EXISTS `tbl_reservation` (
  `reservation_id` int NOT NULL AUTO_INCREMENT,
  `fname` char(50) COLLATE utf8mb4_general_ci NOT NULL,
  `email` char(90) COLLATE utf8mb4_general_ci NOT NULL,
  `phone` char(20) COLLATE utf8mb4_general_ci NOT NULL,
  `total_guests` char(10) COLLATE utf8mb4_general_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `food` char(10) COLLATE utf8mb4_general_ci NOT NULL,
  `entry_date` datetime NOT NULL,
  PRIMARY KEY (`reservation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_reservation`
--

INSERT INTO `tbl_reservation` (`reservation_id`, `fname`, `email`, `phone`, `total_guests`, `datetime`, `food`, `entry_date`) VALUES
(1, 'demo', 'hello@wgdiam.com', '34435435345345', '6', '2023-12-31 12:59:00', 'dinner', '2023-10-18 00:38:18'),
(2, 'demo', 'hello@wgdiam.com', '34435435345345', '6', '2023-12-31 12:59:00', 'dinner', '2023-10-18 00:38:47'),
(3, 'nisatn', 'nishant@gmail.com', '7894561230', '6', '2023-10-22 13:02:00', 'dinner', '2023-10-20 13:59:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_review`
--

DROP TABLE IF EXISTS `tbl_review`;
CREATE TABLE IF NOT EXISTS `tbl_review` (
  `review_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `review` varchar(2000) NOT NULL,
  `datetime` datetime NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_review`
--

INSERT INTO `tbl_review` (`review_id`, `username`, `review`, `datetime`, `path`) VALUES
(18, 'John William', 'With a deep understanding of our client’s business needs areas, extensive resources, and a client people focused approach, Collabera has developed specialized recruiting teams who provide resources in various areas.', '2022-10-06 00:00:00', './admin_asset/review/32ea0dacee64f1c86f829b5230cb9394.jpg'),
(19, 'Mayur Patel', 'Dhillon Printing take care of people because they care! They pay attention to the little things and provide outstanding employee care throughout their journey from the day an offer is made.', '2023-06-28 00:00:00', './admin_asset/review/f40e1630b8794e16238c1b045461b210.jpg'),
(20, 'Herry Tom', 'Dhillon Printing did an incredible job in creating our new marketing & sales pieces. Their prices are very competitive, and their quality is excellent. Thank you for the excellent service on the brochure reprints and the new display posters.', '2023-08-24 00:00:00', './admin_asset/review/6fd690e7baa1584679c03b1c44e1caab.jpg'),
(21, 'John Carter', 'Our client’s business needs areas, extensive resources, and a client people focused approach, Collabera has developed specialized recruiting teams who provide resources in various areas.', '2023-10-02 00:00:00', './admin_asset/review/bb1d941f11e058c10812d53b5291e4d8.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE IF NOT EXISTS `tbl_role` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `remark` varchar(200) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `title`, `remark`) VALUES
(1, 'admin', 'admin role');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

DROP TABLE IF EXISTS `tbl_seo`;
CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `seo_id` int NOT NULL AUTO_INCREMENT,
  `page` varchar(500) NOT NULL,
  `title` varchar(2000) NOT NULL,
  `description` text NOT NULL,
  `keyword` text NOT NULL,
  PRIMARY KEY (`seo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`seo_id`, `page`, `title`, `description`, `keyword`) VALUES
(33, 'index', 'Home', '“Bhojan” is a Hindi word of Sanskrit “Bhojana” that refers to the act of eating or having a meal. It is commonly used in Hindu culture to describe the act of consuming food, often in a ritualistic or ceremonial context.', '“Bhojan” is a Hindi word of Sanskrit “Bhojana” that refers to the act of eating or having a meal. It is commonly used in Hindu culture to describe the act of consuming food, often in a ritualistic or ceremonial context.'),
(34, 'aboutus', 'About us | Bhojan Indian Restaurant', '“Bhojan” is a Hindi word of Sanskrit “Bhojana” that refers to the act of eating or having a meal. It is commonly used in Hindu culture to describe the act of consuming food, often in a ritualistic or ceremonial context.', '“Bhojan” is a Hindi word of Sanskrit “Bhojana” that refers to the act of eating or having a meal. It is commonly used in Hindu culture to describe the act of consuming food, often in a ritualistic or ceremonial context.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slot`
--

DROP TABLE IF EXISTS `tbl_slot`;
CREATE TABLE IF NOT EXISTS `tbl_slot` (
  `slot_id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `slot` int NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL,
  PRIMARY KEY (`slot_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_slot`
--

INSERT INTO `tbl_slot` (`slot_id`, `date`, `time`, `slot`, `entry_date`, `modify_date`) VALUES
(1, '2025-04-20', '22:46:00', 30, '2024-04-05', '2024-04-05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

DROP TABLE IF EXISTS `tbl_submenu`;
CREATE TABLE IF NOT EXISTS `tbl_submenu` (
  `submenu_id` int NOT NULL AUTO_INCREMENT,
  `mainmenu_id` int NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `position` int NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`submenu_id`),
  KEY `Main Menu ID` (`mainmenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_terms`
--

DROP TABLE IF EXISTS `tbl_terms`;
CREATE TABLE IF NOT EXISTS `tbl_terms` (
  `terms_id` int NOT NULL AUTO_INCREMENT,
  `terms` text NOT NULL,
  PRIMARY KEY (`terms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_terms`
--

INSERT INTO `tbl_terms` (`terms_id`, `terms`) VALUES
(5, '<div class=\"elementor-element elementor-element-30949d4 elementor-widget elementor-widget-heading\" data-id=\"30949d4\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">GENERAL</h2></div></div><div class=\"elementor-element elementor-element-81e6a22 elementor-widget elementor-widget-text-editor\" data-id=\"81e6a22\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">If the goods do not match your expectations, please reach out to us to discuss.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">If you have ordered a “kit” but lack the time or motivation to make the product, please reach out to us too.&nbsp; We do make up the final product too and charge this service at a rate of $21 per hour.</p></div></div><div class=\"elementor-element elementor-element-f24c5b4 elementor-widget elementor-widget-heading\" data-id=\"f24c5b4\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">RETURNS</h2></div></div><div class=\"elementor-element elementor-element-54f1482 elementor-widget elementor-widget-text-editor\" data-id=\"54f1482\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Returns are allowed up to 30 days from the date of order. Unfortunately, we cannot offer a refund or exchange after this time.&nbsp; To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">To complete your return, we require the order number/invoice number and the goods to be returned to us.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Please courier your product to 3/4 Carrowmore, Pinehill, Auckland, Auckland, 0632, New Zealand.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of shipping will be deducted from your refund.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Depending on where you live, the time it may take for your exchanged product to reach you may vary.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We suggest you use a trackable shipping service or purchase shipping insurance as we cannot guarantee that we will receive your returned item.</p></div></div><div class=\"elementor-element elementor-element-1d777b7 elementor-widget elementor-widget-text-editor\" data-id=\"1d777b7\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">At this point, we will reach out for your banking information.</p></div></div><div class=\"elementor-element elementor-element-c16ae5d elementor-widget elementor-widget-heading\" data-id=\"c16ae5d\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">REFUNDS</h2></div></div><div class=\"elementor-element elementor-element-22c2aaf elementor-widget elementor-widget-text-editor\" data-id=\"22c2aaf\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">At this point, we will reach out for your banking information.</p></div></div><div class=\"elementor-element elementor-element-9c7a8b2 elementor-widget elementor-widget-heading\" data-id=\"9c7a8b2\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">SALE ITEMS</h2></div></div><div class=\"elementor-element elementor-element-5e1b082 elementor-widget elementor-widget-text-editor\" data-id=\"5e1b082\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Only regular-priced items may be refunded, unfortunately, sale items cannot be refunded.</p></div></div><div class=\"elementor-element elementor-element-06cd5b6 elementor-widget elementor-widget-heading\" data-id=\"06cd5b6\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">EXCHANGES</h2></div></div><div class=\"elementor-element elementor-element-90929a4 elementor-widget elementor-widget-text-editor\" data-id=\"90929a4\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We only replace items if they are defective or damaged.&nbsp; We will consider doing exchanges for incorrect items ordered.&nbsp; Please contact us to see what we can do for you.&nbsp; If you need to exchange it for the same item, send us an email at&nbsp;<a href=\"mailto:sales@clowdercreations.co.nz\" target=\"_blank\" rel=\"noopener\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s; box-shadow: none;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-style: inherit; font-weight: inherit;\">sales@clowdercreations.co.nz</span></font></a><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">&nbsp;and send your item to 3/4 Carrowmore, Pinehill, Auckland, Auckland, 0632, New Zealand.</span></p></div></div><div class=\"elementor-element elementor-element-e74b433 elementor-widget elementor-widget-heading\" data-id=\"e74b433\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">POSTAL ADDRESS</h2></div></div><div class=\"elementor-element elementor-element-947c4b7 elementor-widget elementor-widget-text-editor\" data-id=\"947c4b7\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\">Skin-2-Love 3/4 Carrowmore, Pinehill, 0620</div></div><div class=\"elementor-element elementor-element-6b797aa elementor-widget elementor-widget-heading\" data-id=\"6b797aa\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">EMAIL</h2></div></div><div class=\"elementor-element elementor-element-d85a1e8 elementor-widget elementor-widget-text-editor\" data-id=\"d85a1e8\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><a href=\"mailto:support@skin-2-love.com\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s; box-shadow: none;\">support@skin-2-love.com</a></div></div><div class=\"elementor-element elementor-element-76d0893 elementor-widget elementor-widget-heading\" data-id=\"76d0893\" data-element_type=\"widget\" data-widget_type=\"heading.default\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px; margin-block-end: 20px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><h2 class=\"elementor-heading-title elementor-size-default\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 1; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">PHONE</h2></div></div><div class=\"elementor-element elementor-element-57c1a6d elementor-widget elementor-widget-text-editor\" data-id=\"57c1a6d\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: 22px; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; --flex-direction: initial; --flex-wrap: initial; --justify-content: initial; --align-items: initial; --align-content: initial; --gap: initial; --flex-basis: initial; --flex-grow: initial; --flex-shrink: initial; --order: initial; --align-self: initial; flex-basis: var(--flex-basis); flex-grow: var(--flex-grow); flex-shrink: var(--flex-shrink); order: var(--order); align-self: var(--align-self); flex-direction: var(--flex-direction); flex-wrap: var(--flex-wrap); justify-content: var(--justify-content); align-items: var(--align-items); align-content: var(--align-content); gap: var(--gap); position: relative; --swiper-theme-color: #000; --swiper-navigation-size: 44px; --swiper-pagination-bullet-size: 6px; --swiper-pagination-bullet-horizontal-gap: 6px; --widgets-spacing: 20px 20px; width: 1230px;\"><div class=\"elementor-widget-container\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,transform var(--e-transform-transition-duration,.4s);\"><a href=\"tel:+64 0220798832\" style=\"font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s; box-shadow: none;\">+64 0220798832</a></div></div>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timezone`
--

DROP TABLE IF EXISTS `tbl_timezone`;
CREATE TABLE IF NOT EXISTS `tbl_timezone` (
  `timezone_id` int NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  PRIMARY KEY (`timezone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_timezone`
--

INSERT INTO `tbl_timezone` (`timezone_id`, `identifier`, `timezone`) VALUES
(1, 'Africa', 'Africa/Abidjan'),
(2, 'Africa', 'Africa/Accra'),
(3, 'Asia', 'Asia/Aden'),
(4, 'Asia', 'Asia/Dhaka'),
(5, 'Asia', 'Asia/Almaty'),
(6, 'Asia', 'Asia/Karachi'),
(7, 'Asia', 'Asia/Kathmandu'),
(8, 'Asia', 'Asia/Kolkata'),
(9, 'Asia', 'Asia/Shanghai'),
(10, 'Asia', 'Asia/Tokyo'),
(11, 'Asia', 'Asia/Bangkok');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_web_data`
--

DROP TABLE IF EXISTS `tbl_web_data`;
CREATE TABLE IF NOT EXISTS `tbl_web_data` (
  `web_data_id` int NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `email_address` varchar(500) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `whatsapp` varchar(50) NOT NULL,
  `office` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `map` varchar(1000) NOT NULL,
  `logo` varchar(300) NOT NULL,
  `favicon` varchar(300) NOT NULL,
  `offer_pic` varchar(500) NOT NULL,
  `offer_name` varchar(200) NOT NULL,
  `offer_description` longtext NOT NULL,
  `offer_status` tinyint NOT NULL,
  `footer` varchar(500) NOT NULL,
  `mail_smtp_host` varchar(200) NOT NULL,
  `mail_protocol` varchar(100) NOT NULL,
  `mail_port` int NOT NULL,
  `mail_email` varchar(100) NOT NULL,
  `mail_password` varchar(50) NOT NULL,
  `forgot_mail_subject` varchar(200) NOT NULL,
  `forgot_mail_message` varchar(2000) NOT NULL,
  `welcome_mail_subject` varchar(200) NOT NULL,
  `welcome_mail_message` varchar(2000) NOT NULL,
  `inquiry_mail_subject` varchar(200) NOT NULL,
  `inquiry_mail_message` varchar(2000) NOT NULL,
  `active_mail_subject` varchar(200) NOT NULL,
  `active_mail_message` varchar(2000) NOT NULL,
  `inactive_mail_subject` varchar(200) NOT NULL,
  `inactive_mail_message` varchar(2000) NOT NULL,
  `captcha_site_key` varchar(100) NOT NULL,
  `captcha_secret_key` varchar(100) NOT NULL,
  `captcha_visibility` int NOT NULL,
  `date_format` varchar(50) NOT NULL,
  `time_format` varchar(50) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `meta_keyword` varchar(200) NOT NULL,
  `meta_desc` varchar(200) NOT NULL,
  PRIMARY KEY (`web_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_web_data`
--

INSERT INTO `tbl_web_data` (`web_data_id`, `project_name`, `email_address`, `phone`, `facebook`, `instagram`, `twitter`, `linkedin`, `youtube`, `whatsapp`, `office`, `address`, `map`, `logo`, `favicon`, `offer_pic`, `offer_name`, `offer_description`, `offer_status`, `footer`, `mail_smtp_host`, `mail_protocol`, `mail_port`, `mail_email`, `mail_password`, `forgot_mail_subject`, `forgot_mail_message`, `welcome_mail_subject`, `welcome_mail_message`, `inquiry_mail_subject`, `inquiry_mail_message`, `active_mail_subject`, `active_mail_message`, `inactive_mail_subject`, `inactive_mail_message`, `captcha_site_key`, `captcha_secret_key`, `captcha_visibility`, `date_format`, `time_format`, `timezone`, `meta_keyword`, `meta_desc`) VALUES
(1, 'Shial', 'bhojan@gmail.com', '+91 78744 23415', '#', '#', '#', '#', '#', '', '', 'F-3, 9TH FLOOR, KISHOR PLAZA, B WING, ANAND – 38800', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d59522.03574530071!2d72.783029!3d21.187104!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04deb82d6775d%3A0x9a160866092a9643!2sTrymyloan!5e0!3m2!1sen!2sin!4v1696328600497!5m2!1sen!2sin\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>', './admin_asset/logo/c041261e6c085baaa14036631b1471ac.jpg', './admin_asset/favicon/608eca599f0bb4c3cc175fac58199fb5.jpg', './admin_asset/offer_pic/87bf2d13c244f3e4d2ea4297750fc7df.jpg', 'Black Friday Sale Upto 70%', '<p>The modal plugin toggles your hidden content on demand, via data attributes or JavaScript. It also adds .modal-open to the to override default scrolling behavior and generates a .modal-backdrop to provide a click area for dismissing shown modals when clicking outside the modal.</p><ul><li><b>Veg Item</b></li><li><b>Non Veg Item</b></li><li><b>Drinks</b></li></ul><p>The modal plugin toggles your hidden content on demand, via data attributes or JavaScript. It also adds .modal-open to the to override default scrolling behavior and generates a .modal-backdrop to provide a click area for dismissing shown modals when clicking outside the modal.<br></p>', 1, '©2023 Shial By Divya. All Rights Reserved.', 'smtp', 'ssmtp', 465, 'demo@gmail.com', 'demo123', 'Recover your password - Enzo Admin', '<p>Hello user,</p><p>Click on below link to reset your current password.</p><p><br></p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Register Success - Welcome to Enzo Admin ', '<p>Hello user,</p><p>Firstly Thank you for register with us. You have successfully created account in our admin. You can access all the granted functionality in your account. If you want any further help regarding account, drop your issues on admin mail</p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Inquiry Submitted - Enzo Admin ', '<p>Hello user,</p><p>We have received your inquiry and we look into it very soon.Â </p><p><br></p><p>Thanks & Regards.</p><p><b><i>Enzo Admin.</i></b></p>', 'Profile Activated Successfully - Enzo Admin', '<p>Hello user,</p><p>Congratulations, Your Enzo admin <b>profile is now activated successfully</b>. You are eligible to access our fully functional admin panel.</p><p><br></p><p>Thanks & Regards</p><p><b><i>Enzo Admin.</i></b></p>', 'Your Profile Inactivated - Enzo Admin', '<p>Hello user,</p><p>Your Enzo admin&nbsp;<span style=\"font-weight: bolder;\">profile is now inactivated due to some reason</span>. You are not eligible to access our fully functional admin panel. For more information contact to our admin or team.</p><p><br></p><p>Thanks &amp; Regards</p><p><span style=\"font-weight: bolder;\"><i>Enzo Admin.</i></span></p>', '6LfksB8oAAAAADIqBWS9VBInmh4lmGL73RnLyqQu', '6LfksB8oAAAAAMwwau7hBdvdfsPMo4buMoy3bt8E', 1, 'd-M-Y', 'h', '8', 'TryMyLoan is a company solely focusing on the basic right of every individual to access financial services.\r\nWe are a Surat, Gujarat based company dealing into financial services since 2005.', 'TryMyLoan is a company solely focusing on the basic right of every individual to access financial services.\r\nWe are a Surat, Gujarat based company dealing into financial services since 2005.');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD CONSTRAINT `Main Menu ID` FOREIGN KEY (`mainmenu_id`) REFERENCES `tbl_mainmenu` (`mainmenu_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
